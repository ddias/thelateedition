Rule Commentary_Intro
{
    concept commentary
	target s1commentaryintro
	response Response_Commentary_Intro
}

Response Response_Commentary_Intro
{

	source controller
    remember intro_commentary_over:1
    commentary "Intro Part One"
}

Rule Menu_Opening
{
    concept take
	target menuopening
	response Response_Menu_Opening
}

Response Response_Menu_Opening
{

	source controller
    voice Opening
}

Rule S1_LookAt_Window
{
    concept take
    criteria key_in_window=0 bowler_in_window=0 crowbar_in_window=0
	target window
	response Response_S1_LookAt_Window
}

Response Response_S1_LookAt_Window
{

	source controller
    voice Shadow Apple examine
}

Rule S1_Use_Crowbar_Window
{
    concept combine
    criteria using_crowbar=true
	target window
	response Response_S1_Use_Crowbar_Window
}

Response Response_S1_Use_Crowbar_Window
{

	source window
    remember using_crowbar:-1
    remember crowbar_in_window:1
    voice Shadow use Crowbar on Apple
    imageswap MintsOffice_WindowCrowbar
}

Rule S1_Use_Anything_Window
{
    concept combine
	target apple
	response Response_S1_Use_Anything_Window
}

Response Response_S1_Use_Anything_Window
{

	source controller
    voice Shadow use x on Apple
}

Rule S1_Commentary_Safe_1
{
    concept commentary
	target safe
	criteria safe_commentary=0
	response Response_S1_Commentary_Safe_1
}

Response Response_S1_Commentary_Safe_1
{

	source controller
	remember safe_commentary:1
    commentary Safe commentary
}

Rule S1_Commentary_Safe_2
{
    concept commentary
	target safe
	criteria safe_commentary=1
	response Response_S1_Commentary_Safe_2
}

Response Response_S1_Commentary_Safe_2
{

	source controller
	remember safe_commentary:1
    commentary Crowbar on safe commentary
}

Rule S1_Commentary_Safe_3
{
    concept commentary
	target safe
	criteria safe_commentary=2
	response Response_S1_Commentary_Safe_3
}

Response Response_S1_Commentary_Safe_3
{

	source controller
	remember safe_commentary:-2
    commentary Safe commentary
}

Rule S1_Commentary_Newspaper
{
    concept commentary
	target newspaper
	response Response_S1_Commentary_Newspaper
}

Response Response_S1_Commentary_Newspaper
{

    remember s1_paper_active:1
    commentary Newspaper
}

Rule S1_Commentary_CarpetCorner
{
    concept commentary
    criteria carpet_commentary=0
	target carpetcorner
	response Response_S1_Commentary_CarpetCorner
}

Response Response_S1_Commentary_CarpetCorner
{

	source controller
    remember carpet_commentary:1
    commentary Carpet corner commentary 1
	then carpetcorner commentary
}

Rule S1_Commentary_CarpetCorner2
{
    concept commentary
    criteria carpet_commentary=1
	target carpetcorner
	response Response_S1_Commentary_CarpetCorner2
}

Response Response_S1_Commentary_CarpetCorner2
{

	source controller
    remember carpet_commentary:1
    commentary Carpet corner commentary 2
}

Rule S1_Commentary_CarpetCorner3
{
    concept commentary
    criteria carpet_commentary=1
	target carpetcorner
	response Response_S1_Commentary_CarpetCorner3
}

Response Response_S1_Commentary_CarpetCorner3
{

	source controller
    remember carpet_commentary:-2
    commentary Crowbar looks ridiculous commentary
}


Rule S1_Take_Note
{
    concept take
	target note
	response Response_S1_Take_Note
}

Response Response_S1_Take_Note
{

    remember saw_note:1
}

Rule S1_Unlock_Safe
{
    concept unlocked
	target safe
	response Response_S1_Unlock_Safe
}

Response Response_S1_Unlock_Safe
{

	source safe
    remember key_in_safe:1
    voice s1unlocksafe
    imageswap MintsOffice_FullSafe_Combo
}

Rule S1_Use_Crowbar_Safe
{
    concept combine
    criteria using_crowbar=1 crowbar_in_safe=0
	target safe
	response Response_S1_Use_Crowbar_Safe
}

Response Response_S1_Use_Crowbar_Safe
{

	source safe
    remember using_crowbar:-1
    remember crowbar_in_safe:1
    voice s1usecrowbaronsafe
    imageswap MintsOffice_EmptySafe_Crowbar
}

Rule S1_Use_Door_First
{
    concept take
    criteria used_door=0
	target door
	response Response_S1_Use_Door_First
}

Response Response_S1_Use_Door_First
{

	source controller
    remember used_door:1
    voice s1firstuseddoor
}

Rule S1_Use_Door
{
    concept take
    criteria used_door>1
	target door
	response Response_S1_Use_Door
}

Response Response_S1_Use_Door
{

	source door
    remember used_door:1
    voice s1cantleave
}

Rule S1_Leave_MintsOffice
{
    concept empty
    criteria got_journal=1
	target door
	response Response_S1_Leave_MintsOffice
}

Response Response_S1_Leave_MintsOffice
{

	source controller
    voice s1leave
    scenechange Scene2
}

Rule S1_Get_Journal_From_Trash
{
    concept take
    criteria got_journal=0
	target journal
	response Response_S1_Get_Journal_From_Trash
}

Response Response_S1_Get_Journal_From_Trash
{

	source controller
    remember got_journal:1
    voice s1takejournal
    imageswap MintsOffice_TrashEmpty
}

Rule S1_Take_Files
{
    concept take
    criteria used_files=0
	target files
	response Response_S1_Take_Files
}

Response Response_S1_Take_Files
{

	source controller
    remember used_files:1
    voice s1lookatfiles
}

Rule S1_Use_Sofa
{
    concept take
    criteria used_sofa=0
	target sofa
	response Response_S1_Use_Sofa
}

Response Response_S1_Use_Sofa
{

	source controller
    remember used_sofa:1
    voice s1ahkeys
}

Rule S1_Use_Sofa_Again
{
    concept take
    criteria used_sofa>1
	target sofa
	response Response_S1_Use_Sofa_Again
}

Response Response_S1_Use_Sofa_Again
{

	source controller
    remember used_sofa:1
    voice s1usesofa
}

Rule Commentary_Door_1
{
    concept commentary
	target door
	criteria door_commentary=0
	response Response_Commentary_Door_1
}

Response Response_Commentary_Door_1
{

	source controller
	remember door_commentary:1
    commentary Door commentary
}

Rule Commentary_Door_2
{
    concept commentary
	target door
	criteria door_commentary=1
	response Response_Commentary_Door_2
}

Response Response_Commentary_Door_2
{

	source controller
	remember door_commentary:-1
    commentary S1_Door_Hint
}

Rule Commentary_Window_1
{
    concept commentary
	target window
	criteria window_commentary=0
	response Response_Commentary_Window_1
}

Response Response_Commentary_Window_1
{

	source controller
	remember window_commentary:1
    commentary Window commentary
}

Rule Commentary_Window_2
{
    concept commentary
	target window
	criteria window_commentary=1
	response Response_Commentary_Window_2
}

Response Response_Commentary_Window_2
{

	source controller
	remember window_commentary:1
    commentary Apple on ledge commentary
}

Rule Commentary_Window_3
{
    concept commentary
	target window
	criteria window_commentary=2
	response Response_Commentary_Window_3
}

Response Response_Commentary_Window_3
{

	source controller
	remember window_commentary:1
    commentary Bowler and key on window commentary
}

Rule Commentary_Window_4
{
    concept commentary
	target window
	criteria window_commentary=3
	response Response_Commentary_Window_4
}

Response Response_Commentary_Window_4
{

	source controller
	remember window_commentary:-3
    commentary Something odd commentary
}

Rule S1_Use_Apple_CrowbarWindow
{
    concept combine
    criteria using_apple=true crowbar_in_window=true
	target window
	response Response_S1_Use_Apple_CrowbarWindow
}

Response Response_S1_Use_Apple_CrowbarWindow
{

	source window
    remember using_apple:-1
    remember crowbar_in_window:-1
    imageswap MintsOffice_WindowClosed
}

Rule S1_Use_Bowler_Window
{
    concept combine
    criteria using_bowler=true crowbar_in_window=false key_in_window=false
	target window
	response Response_S1_Use_Bowler_Window
}

Response Response_S1_Use_Bowler_Window
{

	source window
    remember using_bowler:-1
    remember bowler_in_window:1
    imageswap MintsOffice_WindowBowler
}

Rule S1_Use_Key_Window
{
    concept combine
    criteria using_floorkey=true crowbar_in_window=false bowler_in_window=false
	target window
	response Response_S1_Use_Key_Window
}

Response Response_S1_Use_Key_Window
{

	source window
    remember using_floorkey:-1
    remember key_in_window:1
    imageswap MintsOffice_WindowKey
}

Rule S1_Use_CarpetCorner_Crowbar
{
    concept combine
    criteria key_in_rug=0 apple_in_rug=0 using_crowbar=1
	target carpetcorner
	response Response_S1_Use_CarpetCorner_Crowbar
}

Response Response_S1_Use_CarpetCorner_Crowbar
{

	source carpetcorner
    remember crowbar_in_rug:1
    remember using_crowbar:-1
	voice ADD_cartpet
    imageswap MintsOffice_Rug_DoorReveal
}

Rule S1_Use_CarpetCorner_Apple
{
    concept combine
    criteria using_apple=1 crowbar_in_rug=1 key_in_rug=1 apple_in_rug=0
	target carpetcorner
	response Response_S1_Use_CarpetCorner_Apple
}

Response Response_S1_Use_CarpetCorner_Apple
{

	source carpetcorner
    remember using_apple:-1
    remember apple_in_rug:1
    imageswap MintsOffice_Rug_Door_Opened
}

Rule S1_Use_CarpetCorner_Key
{
    concept combine
    criteria using_floorkey=1 crowbar_in_rug=1 apple_in_rug=0 key_in_rug=0
	target carpetcorner
	response Response_S1_Use_CarpetCorner_Key
}

Response Response_S1_Use_CarpetCorner_Key
{

	source carpetcorner
    remember using_floorkey:-1
    remember key_in_rug:1
    imageswap MintsOffice_Rug_Door_Key
}

Rule S1_Use_CarpetCorner_KeyAlt
{
    concept combine
    criteria using_crowbar=1 crowbar_in_rug=0 apple_in_rug=0 key_in_rug=1
	target carpetcorner
	response Response_S1_Use_CarpetCorner_KeyAlt
}

Response Response_S1_Use_CarpetCorner_KeyAlt
{

	source carpetcorner
    remember using_crowbar:-1
    remember crowbar_in_rug:1
    imageswap MintsOffice_Rug_Door_Key
}

Rule S1_Use_CarpetCorner_RemoveCrowbar
{
    concept take
    criteria crowbar_in_rug=1
	target crowbar
	response Response_S1_Use_CarpetCorner_RemoveCrowbar
}

Response Response_S1_Use_CarpetCorner_RemoveCrowbar
{

	source carpetcorner
    remember crowbar_in_rug:-1
    imageswap MintsOffice_Rug
}



Rule S2_Commentary_Colosus_to_dwarf_1
{
    concept commentary
    criteria Colosus_State=0
	target Colosus
	response Response_S2_Commentary_to_dwarf_1
}

Response Response_S2_Commentary_to_dwarf_1
{

    remember Colosus_State:1
    commentary "S2_Colosus_to_Dwarf_1"
}


Rule S2_Commentary_Colosus_to_norm_1
{
    concept commentary
    criteria Colosus_State=1
	target Colosus
	response Response_S2_Commentary_to_norm_1
}

Response Response_S2_Commentary_to_norm_1
{

    remember Colosus_State:1
    commentary "S2_Colosus_to_Humble_1"
}

Rule S2_Commentary_Colosus_to_russ_1
{
    concept commentary
    criteria Colosus_State=2
	target Colosus
	response Response_S2_Commentary_to_russ_1
}

Response Response_S2_Commentary_to_russ_1
{

    remember Colosus_State:1
    commentary "Commentary Colossus Russian first"
}


Rule S2_Commentary_Colosus_to_dwarf_2
{
    concept commentary
    criteria Colosus_State=3
	target Colosus
	response Response_S2_Commentary_to_dwarf_2
}

Response Response_S2_Commentary_to_dwarf_2
{

    remember Colosus_State:1
    commentary "S2_Colosus_to_Dwarf_2"
}


Rule S2_Commentary_Colosus_to_norm_2
{
    concept commentary
    criteria Colosus_State=4
	target Colosus
	response Response_S2_Commentary_to_norm_2
}

Response Response_S2_Commentary_to_norm_2
{

    remember Colosus_State:1
    commentary "S2_Colosus_to_Humble_2"
}

Rule S2_Commentary_Colosus_to_russ_2
{
    concept commentary
    criteria Colosus_State=5
	target Colosus
	response Response_S2_Commentary_to_russ_2
}

Response Response_S2_Commentary_to_russ_2
{

    remember Colosus_State:1
    commentary "Commentary Colossus Russian"
}


Rule S2_Commentary_Colosus_to_dwarf_3
{
    concept commentary
    criteria Colosus_State=6
	target Colosus
	response Response_S2_Commentary_to_dwarf_3
}

Response Response_S2_Commentary_to_dwarf_3
{

    remember Colosus_State:1
    commentary "RPC_to_dwarf"
}


Rule S2_Commentary_Colosus_to_norm_3
{
    concept commentary
    criteria Colosus_State=7
	target Colosus
	response Response_S2_Commentary_to_norm_3
}

Response Response_S2_Commentary_to_norm_3
{

    remember Colosus_State:1
    commentary "RPC_to_pchy"
}

Rule S2_Commentary_Colosus_to_russ_3
{
    concept commentary
    criteria Colosus_State=8
	target Colosus
	response Response_S2_Commentary_to_russ_3
}

Response Response_S2_Commentary_to_russ_3
{

    remember Colosus_State:-2
    commentary "RPC_to_russianf"
}




Rule S2_Commentary_KeyTranspose
{
    concept commentary
    criteria key_to_transpose=0
	target KeyTranspose
	response Response_S2_Commentary_KeyTranspose
}

Response Response_S2_Commentary_KeyTranspose
{

    remember key_to_transpose:1
}

Rule S1_Take_Report1
{
    concept take
	target report1
	response Response_S1_Take_Report1
}

Response Response_S1_Take_Report1
{

	source controller
    remember got_report1:1
}

Rule S1_Take_Report2
{
    concept take
	target report2
	response Response_S1_Take_Report2
}

Response Response_S1_Take_Report2
{

	source controller
    remember got_report2:1
}

Rule S1_Use_Apple_FloorkeyWindow
{
    concept combine
    criteria using_apple=1 key_in_window=1
	target window
	response Response_S1_Use_Apple_FloorkeyWindow
}

Response Response_S1_Use_Apple_FloorkeyWindow
{

	source window
    remember using_apple:-1
    remember key_in_window:-1
    imageswap MintsOffice_WindowClosed
}

Rule S1_Use_Apple_BowlerWindow
{
    concept combine
    criteria using_apple=1 bowler_in_window=1
	target window
	response Response_S1_Use_Apple_BowlerWindow
}

Response Response_S1_Use_Apple_BowlerWindow
{

	source window
    remember using_apple:-1
    remember bowler_in_window:-1
    imageswap MintsOffice_WindowClosed
}

Rule S1_Take_Newspaper
{
    concept take
	target newspaper
	response Response_S1_Take_Newspaper
}

Response Response_S1_Take_Newspaper
{

	source controller
    voice Shadow wants Apple
}

Rule S1_Use_Key_Safe
{
    concept combine
    criteria using_floorkey=1 crowbar_in_safe=1
	target safe
	response Response_S1_Use_Key_Safe
}

Response Response_S1_Use_Key_Safe
{

	source safe
    remember using_floorkey:-1
    remember crowbar_in_safe:-1
    imageswap MintsOffice_Safe
}

Rule S1_Take_From_Safe
{
    concept take
    criteria key_in_safe=1 crowbar_in_safe=0
	target safe
	response Response_S1_Take_From_Safe
}

Response Response_S1_Take_From_Safe
{

	source safe
    remember key_in_safe:-1
    imageswap MintsOffice_EmptySafe_Combo
}

Rule S2_Intro_1
{
    concept commentary
    criteria intro_queue=0
	target scene2_intro
	response Response_S2_Intro_1
}

Response Response_S2_Intro_1
{

    remember intro_queue:1
    commentary "Commentary 1"
}

Rule S2_Intro_1p5
{
    concept commentary
    criteria intro_queue=1
	target scene2_intro
	response Response_S2_Intro_1p5
}

Response Response_S2_Intro_1p5
{

    remember intro_queue:1
    commentary "S2_Colosus_Commentary_Bandage_This_Up"
}

Rule S2_Intro_2
{
    concept commentary
    criteria intro_queue=2
	target scene2_intro
	response Response_S2_Intro_2
}

Response Response_S2_Intro_2
{

    remember intro_queue:1
    commentary "Commentary 4"
}

Rule S2_Intro_3
{
    concept commentary
    criteria intro_queue=3
	target scene2_intro
	response Response_S2_Intro_3
}

Response Response_S2_Intro_3
{

    remember intro_queue:1
    commentary "Commentary 5"
}

Rule S2_Intro_4
{
    concept commentary
    criteria intro_queue=4
	target scene2_intro
	response Response_S2_Intro_4
}

Response Response_S2_Intro_4
{

    remember intro_queue:1
    commentary "Commentary 6"
}

Rule S2_Intro_5
{
    concept commentary
    criteria intro_queue=5
	target scene2_intro
	response Response_S2_Intro_5
}

Response Response_S2_Intro_5
{

    remember intro_queue:-5
    commentary "Commentary 10"
}

Rule S2_Rosebud_0
{
    concept commentary
    criteria rosebud_queue=0
	target commentary_rosebud
	response Response_S2_Rosebud_0
}

Response Response_S2_Rosebud_0
{

    remember rosebud_queue:1
    commentary "Commentary 7"
}

Rule S2_Rosebud_1
{
    concept commentary
    criteria rosebud_queue=2
	target commentary_rosebud
	response Response_S2_Rosebud_1
}

Response Response_S2_Rosebud_1
{

    remember rosebud_queue:1
    commentary "Commentary 8"
}

Rule S2_Rosebud_2
{
    concept commentary
    criteria rosebud_queue=3
	target commentary_rosebud
	response Response_S2_Rosebud_2
}

Response Response_S2_Rosebud_2
{

    remember rosebud_queue:-3
    commentary "Commentary 9"
}


Rule S2_Hungry
{
    concept commentary
	criteria S2_Hungry=0
	target commentary_hungry
	response Response_S2_Hungry
}

Response Response_S2_Hungry
{
	remember S2_Hungry:1
    commentary "Commentary 3"
}

Rule S2_Hungry_2
{
    concept commentary
	criteria S2_Hungry=1
	target commentary_hungry
	response Response_S2_Hungry_2
}

Response Response_S2_Hungry_2
{
	remember S2_Hungry:-1
    commentary "S2_Feet_First_Flight"
}


Rule S2_First_Shaddow
{
    concept take
	criteria S2_First_Shaddow=0
	target background
	response Response_S2_First_Shaddow
}

Response Response_S2_First_Shaddow
{
	source controller
	remember S2_First_Shaddow:1
    commentary "Shadow first Colossus"
}



Rule S2_who_is_colosus
{
    concept commentary
	criteria who_is_colosus=0
	target commentary_who_is_colosus
	response Response_S2_who_is_colosus
}

Response Response_S2_who_is_colosus
{
	remember who_is_colosus:1
    commentary "Commentary Colossus 2"
}

Rule S2_who_is_colosus_2
{
    concept commentary
	criteria who_is_colosus=1
	target commentary_who_is_colosus
	response Response_S2_who_is_colosus_2
}

Response Response_S2_who_is_colosus_2
{
	remember who_is_colosus:1
    commentary "S2_Colosus_Commentary_Bandage_This_Up"
}

Rule S2_who_is_colosus_3
{
    concept commentary
	criteria who_is_colosus=2
	target commentary_who_is_colosus
	response Response_S2_who_is_colosus_3
}

Response Response_S2_who_is_colosus_3
{
	remember who_is_colosus:-2
    commentary "Commentary Colossus 3"
}



Rule S2_sheakspearDouche
{
    concept commentary
    criteria rosebud_queue=1
	target commentary_rosebud
	response Response_S2_sheakspearDouche
}

Response Response_S2_sheakspearDouche
{
	remember rosebud_queue:1
    commentary "S2_Colosus_Commentary_Sheakspear_Plus_Batman_eq_Douche"
}


Rule S2_commentary_glass
{
    concept commentary
	criteria tempest_glass_queue=0
	target commentary_tempest_glass
	response Response_S2_commentary_glass
}

Response Response_S2_commentary_glass
{
	remember tempest_glass_queue:1
    commentary "Voice commentary"
}

Rule S2_commentary_tempest
{
    concept commentary
	criteria tempest_glass_queue=1
	target commentary_tempest_glass
	response Response_S2_commentary_tempest
}

Response Response_S2_commentary_tempest
{
	remember tempest_glass_queue:1
    commentary "Commentary Shakespeare revenge"
}


Rule S2_commentary_blownoutvoice
{
    concept commentary
	criteria tempest_glass_queue=2
	target commentary_tempest_glass
	response Response_S2_commentary_blownoutvoice
}

Response Response_S2_commentary_blownoutvoice
{
	remember tempest_glass_queue:-2
    commentary "blownoutvoice"
}




Rule S2_key_transpose
{
    concept commentary
	criteria key_transpose_queue=0
	target key_transpose
	response Response_S2_key_transpose
}

Response Response_S2_key_transpose
{
	remember key_transpose_queue:1
    commentary "Commentary car keys"
}



Rule S2_key_transpose_2
{
    concept commentary
	criteria key_transpose_queue=1
	target key_transpose
	response Response_S2_key_transpose_2
}

Response Response_S2_key_transpose_2
{
	remember key_transpose_queue:-1
    commentary "Journal 1"
}

Rule S2_scene2_comment_gun
{
    concept commentary
	criteria scene2_comment_gun_queue=0
	target scene2_comment_gun
	response Response_S2_scene2_comment_gun
}

Response Response_S2_scene2_comment_gun
{
	remember scene2_comment_gun_queue:1
    commentary "Commentary gun"
}

Rule S2_scene2_comment_gun_2
{
    concept commentary
	criteria scene2_comment_gun_queue=1
	target scene2_comment_gun
	response Response_S2_scene2_comment_gun_2
}

Response Response_S2_scene2_comment_gun_2
{
	remember scene2_comment_gun_queue:1
    commentary "Tired_with_a_capital_T"
}

Rule S2_scene2_comment_master_hint
{
    concept commentary
	criteria scene2_comment_gun_queue=2
	target scene2_comment_gun
	response Response_S2_scene2_comment_master_hint
}

Response Response_S2_scene2_comment_master_hint
{
	remember scene2_comment_gun_queue:-2
    commentary "gunboozejournal"
}




Rule S2_Col_Rus_cp1
{
    concept talk
	criteria Colosus_State=0 CommentaryMode=0
	target Colosus
	response Response_S2_Col_Rus_cp1
}

Response Response_S2_Col_Rus_cp1
{
	source controller
    voice "Colossus Russian 1F"
}

Rule S2_Col_Rus_cp2
{
    concept talk
	criteria Colosus_State=3 CommentaryMode=0
	target Colosus
	response Response_S2_Col_Rus_cp2
}

Response Response_S2_Col_Rus_cp2
{
	source controller
    voice "Colossus Russian 1F"
}

Rule S2_Col_Rus_cp3
{
    concept talk
	criteria Colosus_State=6 CommentaryMode=0
	target Colosus
	response Response_S2_Col_Rus_cp3
}

Response Response_S2_Col_Rus_cp3
{
	source controller
    voice "Colossus Russian 1F"
}



Rule S2_Col_Rus_2_cp1
{
    concept combine
	criteria Colosus_State=0 using_booze=1
	target Colosus
	response Response_S2_Col_Rus_2_cp1
}

Response Response_S2_Col_Rus_2_cp1
{
	source controller
    voice "Colossus Russian 2F"
}

Rule S2_Col_Rus_2_cp2
{
    concept combine
	criteria Colosus_State=3 using_booze=1
	target Colosus
	response Response_S2_Col_Rus_2_cp2
}

Response Response_S2_Col_Rus_2_cp2
{
	source controller
    voice "Colossus Russian 2F"
}

Rule S2_Col_Rus_2_cp3
{
    concept combine
	criteria Colosus_State=6 using_booze=1
	target Colosus
	response Response_S2_Col_Rus_2_cp3
}

Response Response_S2_Col_Rus_2_cp3
{
	source controller
    voice "Colossus Russian 2F"
}





Rule S2_Col_brit_1_cp1
{
    concept talk
	criteria Colosus_State=2 CommentaryMode=0
	target Colosus
	response Response_S2_Col_brit_1_cp1
}

Response Response_S2_Col_brit_1_cp1
{
	source controller
    voice "Colossus British 1F"
}



Rule S2_Col_brit_1_cp2
{
    concept talk
	criteria Colosus_State=5 CommentaryMode=0
	target Colosus
	response Response_S2_Col_brit_1_cp2
}

Response Response_S2_Col_brit_1_cp2
{
	source controller
    voice "Colossus British 1F"
}



Rule S2_Col_brit_1_cp3
{
    concept talk
	criteria Colosus_State=8 CommentaryMode=0
	target Colosus
	response Response_S2_Col_brit_1_cp3
}

Response Response_S2_Col_brit_1_cp3
{
	source controller
    voice "Colossus British 1F"
}




Rule S2_Col_brit_gunned_cp1
{
    concept combine
	criteria Colosus_State=2 using_gun=1
	target Colosus
	response Response_S2_Col_brit_gunned_cp1
}

Response Response_S2_Col_brit_gunned_cp1
{
	source controller
    voice "Colossus British 2F"
}



Rule S2_Col_brit_gunned_cp2
{
    concept combine
	criteria Colosus_State=5 using_gun=1
	target Colosus
	response Response_S2_Col_brit_gunned_cp2
}

Response Response_S2_Col_brit_gunned_cp2
{
	source controller
    voice "Colossus British 2F"
}



Rule S2_Col_brit_gunned_cp3
{
    concept combine
	criteria Colosus_State=8 using_gun=1
	target Colosus
	response Response_S2_Col_brit_gunned_cp3
}

Response Response_S2_Col_brit_gunned_cp3
{
	source controller
    voice "Colossus British 2F"
}





Rule S2_Col_scot_1_cp1
{
    concept talk
	criteria Colosus_State=1 CommentaryMode=0 colosus_boozed=0
	target Colosus
	response Response_S2_Col_scot_1_cp1
}

Response Response_S2_Col_scot_1_cp1
{
	source controller
    voice "Colossus Scottish 1F"
}



Rule S2_Col_scot_1_cp2
{
    concept talk
	criteria Colosus_State=4 CommentaryMode=0 colosus_boozed=0
	target Colosus
	response Response_S2_Col_scot_1_cp2
}

Response Response_S2_Col_scot_1_cp2
{
	source controller
    voice "Colossus Scottish 1F"
}



Rule S2_Col_scot_1_cp3
{
    concept talk
	criteria Colosus_State=7 CommentaryMode=0 colosus_boozed=0
	target Colosus
	response Response_S2_Col_scot_1_cp3
}

Response Response_S2_Col_scot_1_cp3
{
	source controller
    voice "Colossus Scottish 1F"
}




Rule S2_Col_scot_drunk_cp1
{
    concept talk
	criteria Colosus_State=1 CommentaryMode=0 colosus_boozed=1
	target Colosus
	response Response_S2_Col_scot_drunk_cp1
}

Response Response_S2_Col_scot_drunk_cp1
{
	source controller
    voice "Colossus Scottish 2F"
}

Rule S2_Col_scot_drunk_cp2
{
    concept talk
	criteria Colosus_State=4 CommentaryMode=0 colosus_boozed=1
	target Colosus
	response Response_S2_Col_scot_drunk_cp2
}

Response Response_S2_Col_scot_drunk_cp2
{
	source controller
    voice "Colossus Scottish 2F"
}

Rule S2_Col_scot_drunk_cp3
{
    concept talk
	criteria Colosus_State=7 CommentaryMode=0 colosus_boozed=1
	target Colosus
	response Response_S2_Col_scot_drunk_cp3
}

Response Response_S2_Col_scot_drunk_cp3
{
	source controller
    voice "Colossus Scottish 2F"
}



Rule S2_Col_scot_journal_cp1
{
    concept combine
	criteria Colosus_State=1 colosus_boozed=1 using_journal=1
	target Colosus
	response Response_S2_Col_scot_journal_cp1
}

Response Response_S2_Col_scot_journal_cp1
{
	source controller
    voice "Colossus Scottish 3F"
}

Rule S2_Col_scot_journal_cp2
{
    concept combine
	criteria Colosus_State=4 colosus_boozed=1 using_journal=1
	target Colosus
	response Response_S2_Col_scot_journal_cp2
}

Response Response_S2_Col_scot_journal_cp2
{
	source controller
    voice "Colossus Scottish 3F"
}

Rule S2_Col_scot_journal_cp3
{
    concept combine
	criteria Colosus_State=7 colosus_boozed=1 using_journal=1
	target Colosus
	response Response_S2_Col_scot_journal_cp3
}

Response Response_S2_Col_scot_journal_cp3
{
	source controller
    voice "Colossus Scottish 3F"
}


Rule S2_commentary_janitorscloset
{
    concept commentary
	target janitorscloset
	response Response_S2_commentary_janitorscloset
}

Response Response_S2_commentary_janitorscloset
{
    commentary "janitorscloset"
}


Rule S2_commentary_artofgold
{
    concept commentary
	target s2_exit
	response Response_S2_commentary_artofgold
}

Response Response_S2_commentary_artofgold
{
    commentary "artofdollars"
}




Rule S2_commentary_stool
{
    concept commentary
	criteria stool_queue=0
	target stool
	response Response_S2_commentary_stool
}

Response Response_S2_commentary_stool
{
	remember stool_queue:1
    commentary "Drawing Stool"
}

Rule S2_commentary_stool_2
{
    concept commentary
	criteria stool_queue=1
	target stool
	response Response_S2_commentary_stool_2
}

Response Response_S2_commentary_stool_2
{
	remember stool_queue:-1
    commentary "S1_Stool_2"
}

Rule S2_commentary_stool_shd
{
    concept take
	target stool
	response Response_S2_commentary_stool_shd
}

Response Response_S2_commentary_stool_shd
{
    voice "Shadow Stool"
}

Rule S1_commentary_Ben_Intro
{
    concept commentary
	criteria hinter_queue=0
	target commentary_hinter
	response Response_S1_commentary_Ben_Intro
}

Response Response_S1_commentary_Ben_Intro
{
	remember hinter_queue:1
    commentary "S1_Ben_Intro"
}

Rule S1_commentary_fedora_factor
{
    concept commentary
	criteria hinter_queue=1
	target commentary_hinter
	response Response_S1_commentary_fedora_factor
}

Response Response_S1_commentary_fedora_factor
{
	remember hinter_queue:1
    commentary "S1_Fedora_Facctor"
}

Rule S1_commentary_hinter
{
    concept commentary
	criteria hinter_queue=2
	target commentary_hinter
	response Response_S1_commentary_hinter
}

Response Response_S1_commentary_hinter
{
	remember hinter_queue:1
    commentary "top_ramen_impact"
}

Rule S1_commentary_Massive_Hint
{
    concept commentary
	criteria hinter_queue=3
	target commentary_hinter
	response Response_S1_commentary_Massive_Hint
}

Response Response_S1_commentary_Massive_Hint
{
	remember hinter_queue:-3
    commentary "S1_Massive_Hint"
}

Rule S1_commentary_cabinet
{
    concept commentary
	target cabinetz
	response Response_S1_commentary_cabinet
}

Response Response_S1_commentary_cabinet
{
    commentary "S1_Cabinets"
}

Rule S1_shadow_cabinet
{
    concept take
	target cabinetz
	response Response_S1_shadow_cabinet
}

Response Response_S1_shadow_cabinet
{
    commentary "S1_Shadow_Cabintes"
}

Rule S1_commentary_carpet
{
    concept commentary
	target carpet
	response Response_S1_commentary_carpet
}

Response Response_S1_commentary_carpet
{
    commentary "S1_The_Carpet_art"
}

Rule S1_desk_crowbar
{
    concept take
	target desk
	response Response_S1_desk_crowbar
}

Response Response_S1_desk_crowbar
{
    commentary "ADD_blackjack"
}

Rule S1_End
{
    concept take
	target s2_exit
	response Response_End
}

Response Response_End
{
    commentary "Outro"
}
