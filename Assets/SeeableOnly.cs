﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using EventMaster;

public class SeeableOnly : Container
{
    public override void TryCombine(GameObject obj)
    {
        CombineFail(obj); // this will call this's query(combine) and then obj's combine fail (might call query(combine) ) 
    }

    public override void Take()
    {
        //perform 'take' to trigger character voices - do nothing else
        Debug.Log("Container::Take()");
        GetComponent<EventParticipant>().PerformQuery("take");
    }
}
