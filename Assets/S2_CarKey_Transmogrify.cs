﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using EventMaster;

public class S2_CarKey_Transmogrify : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        Memory.Instance.memory["key_to_transpose"] = 0;
        KeyTransferer.SetActive(false); //@@@bad place to do this
    }
	
	// Update is called once per frame
	void Update () {
        int read_key_to_transpose = (int)Memory.Instance.memory["key_to_transpose"];
        
        //this allows you to transpose the keys - I really want the keys to automatically disapear, but this will work.
        if (read_key_to_transpose == 1)
        {
            KeyTransferer.SetActive(true);
            Memory.Instance.memory["key_to_transpose"] = 0;
        }
    }

    public void Unhide() {
        KeyTransferer.SetActive(true);
    }

    public GameObject KeyTransferer;
}
