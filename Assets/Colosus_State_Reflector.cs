﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using EventMaster;

public class Colosus_State_Reflector : MonoBehaviour {

    public void Awake()
    {
        Memory.Instance.memory["Colosus_State"] = 0; //starts as regular
        Memory.Instance.memory["colosus_boozed"] = 0;
    }

    public S2_Colosus UpdateTarget;
    void Update()
    {
        //Debug.LogFormat("{0}, {1}", Memory.Instance.memory["CommentaryMode"],Memory.Instance.memory["Colosus_State"]);
        float stof = Memory.Instance.memory["Colosus_State"];
        int sto = (int)stof;

        // russian state
        if ((sto == 0) || (sto == 3) || (sto == 6))
            
        {
            UpdateTarget.isregular = false;
            UpdateTarget.isrussian = true;
            UpdateTarget.isdwarf = false;
        }
        // dwarf state
        else if ((sto == 1) || (sto == 4) || (sto == 7))
        {
            UpdateTarget.isregular = false;
            UpdateTarget.isrussian = false;
            UpdateTarget.isdwarf = true;
        }
        // regular state
        else if ((sto == 2) || (sto == 5) || (sto == 8))
        {
            UpdateTarget.isregular = true;
            UpdateTarget.isrussian = false;
            UpdateTarget.isdwarf = false;
        }
        // regular state
        else
        {
            UpdateTarget.isregular = true;
            UpdateTarget.isrussian = false;
            UpdateTarget.isdwarf = false;
        }
    }

}
