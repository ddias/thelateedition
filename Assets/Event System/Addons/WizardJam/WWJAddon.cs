﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using EventMaster;

public class WWJAddon : BaseAddon
{
    public WWJAddon(): base()
	{
		ResponsePhrases.Add("voice",ScriptParser.StringListAppendProcessor);
        ResponsePhrases.Add("commentary", ScriptParser.StringListAppendProcessor);
        ResponsePhrases.Add("sfx", ScriptParser.StringListAppendProcessor);
        ResponsePhrases.Add("imageswap", ScriptParser.StringListAppendProcessor);
        ResponsePhrases.Add("scenechange", ScriptParser.StringListAppendProcessor);

        ResponseVariables.Add("voice", new List<string>());
        ResponseVariables.Add("commentary", new List<string>());
        ResponseVariables.Add("sfx", new List<string>());
        ResponseVariables.Add("imageswap", new List<string>());
        ResponseVariables.Add("scenechange", new List<string>());

        Helper.Add("voice", "Sound file ID");
        Helper.Add("commentary", "Sound file ID");
        Helper.Add("sfx", "Sound file ID");
        Helper.Add("imageswap", "Image name");
        Helper.Add("scenechange", "Scene name");
    }

    public override void ResolveResponse(Response res)
    {
        List<string> voice = (List<string>)res.addonStorage["voice"];
        List<string> commentary = (List<string>)res.addonStorage["commentary"];
        List<string> sfx = (List<string>)res.addonStorage["sfx"];
        List<string> imageswap = (List<string>)res.addonStorage["imageswap"];
        List<string> scene = (List<string>)res.addonStorage["scenechange"];
        // TODO: This is hacky, may slow stuff down... may not matter
        GameObject controller = GameObject.Find("Controller");
        // TODO: Pick random one? Just pick first for now.
        if (voice.Count > 0)
            ExecuteEvents.Execute<ISoundPlayerTarget>(controller, null, (x, y) => x.Play(voice[0]));
        if (commentary.Count > 0)
            ExecuteEvents.Execute<ISoundPlayerTarget>(controller, null, (x, y) => x.PlayCommentary(commentary[0]));
        if (sfx.Count > 0)
            ExecuteEvents.Execute<ISoundPlayerTarget>(controller, null, (x, y) => x.PlaySFX(sfx[0]));
        if (imageswap.Count > 0)
            ExecuteEvents.Execute<ISpriteSwapHandler>(controller,null,(x,y) => x.SwapImage(res.source,imageswap[0]));
        if (scene.Count > 0)
            ExecuteEvents.Execute<ISceneSwitcherTarget>(controller, null, (x, y) => x.LoadScene(scene[0]));

        base.ResolveResponse(res);
    }
}