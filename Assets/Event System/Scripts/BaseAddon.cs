﻿using UnityEngine;
using System.Collections.Generic;

namespace EventMaster
{
	[ExecuteInEditMode()]
	public class BaseAddon
	{
		/// <summary>
		/// Add to this collection to add to the scripting languages supported Rule keywords
		/// </summary>
		public Dictionary<string,ScriptParser.PhraseProcessor> RulePhrases { get; internal set; }
		/// <summary>
		/// Add to this collection to add to the scripting languages supported Response keywords
		/// </summary>
		public Dictionary<string,ScriptParser.PhraseProcessor> ResponsePhrases  { get; internal set; }
		
		/// <summary>
		/// Add to this collection to setup Rule keyword data type stored when parsing
		/// </summary>
		public Dictionary<string,object> RuleVariables  { get; internal set; }
		/// <summary>
		/// Add to this collection to setup Response keyword data type stored when parsing
		/// </summary>
		public Dictionary<string,object> ResponseVariables  { get; internal set; }

		/// <summary>
		/// String to show users formatting in the editor when they are entering phrases.
		/// </summary>
		public Dictionary<string,string> Helper { get; internal set; }
	
		public BaseAddon()
		{
			RulePhrases = new Dictionary<string, ScriptParser.PhraseProcessor>();
			ResponsePhrases = new Dictionary<string, ScriptParser.PhraseProcessor>();
			RuleVariables = new Dictionary<string, object>();
			ResponseVariables = new Dictionary<string, object>();
			Helper = new Dictionary<string, string>();
		}
		
		/// <summary>
		/// Use this function to extend the string to being hashed to match or store in the parser
		/// </summary>
		/// <returns>The hash</returns>
		public virtual string ExtendHash()
		{
			// By default add nothing.
			return "";
		}
		
		/// <summary>
		/// Called after the response is parsed.
		/// </summary>
		/// <param name="response">Response</param>
		public virtual void AfterResponseParsed(string name, Response response)
		{
		
		}
		
		/// <summary>
		/// Called after the rule is parsed.
		/// </summary>
		/// <param name="rule">Rule.</param>
		public virtual void AfterRuleParsed(string name, Rule rule)
		{
			
		}
		
		/// <summary>
		/// Use this function to add custom code when a response is found by the query engine
		/// </summary>
		/// <param name="res">Response data</param>
		public virtual void ResolveResponse(Response res)
		{
			
		}
	}
}