﻿using UnityEngine;
using System.Collections.Generic;

namespace EventMaster
{
	public class CriterionStatic {
		// any value less than this and our comparer doesnt work 0.0000001f
		public static double Epsilon = 0.0000001f;

		public string var { get; internal set; }
		double a, fa, fb;
		EvalOperator op;
		
		public CriterionStatic(string v, EvalOperator o, double a)
		{
			var = v;
			this.a = a;
			op = o;
			switch(o)
			{
			case EvalOperator.Equals:
				fa = a;
				fb = a + CriterionStatic.Epsilon;
				break;
			case EvalOperator.NotEquals:
				// TODO Figure out how to really do this! I don't see how it's possible but the GDC talk says that it is. I think it's lies.
				// http://www.gdcvault.com/play/1015317/AI-driven-Dynamic-Dialog-through
				//fa = a+CriterionStatic.Epsilon;
				//fb = a-CriterionStatic.Epsilon;
				fa = a;
				break;
			case EvalOperator.LessThanEqual:
				fa = double.MinValue;
				fb = a;
				break;
			case EvalOperator.LessThan:
				fa = double.MinValue;
				fb = a-CriterionStatic.Epsilon;
				break;
			case EvalOperator.GreaterThanEqual:
				fa = a;
				fb = double.MaxValue;
				break;
			case EvalOperator.GreaterThan:
				fa = a+CriterionStatic.Epsilon;
				fb = double.MaxValue;
				break;
			case EvalOperator.IsTrue:
				fa = 1;
				fb = double.MaxValue;
				break;
			case EvalOperator.IsFalse:
				fa = 0;
				fb = 0;
				break;
			}
		}
		
		public bool Compare(double x) {
			if(op != EvalOperator.NotEquals)
			{
				//Debug.Log("Evaluating: "+var+"... \t"+x+" >= "+fa+" && "+fb+" >= "+x+"\n\t\t\t\t\t\t\t\t\t\t"+(x>=fa).ToString()+" && "+(fb>=x).ToString());
				return x >= fa && fb >= x;
			}
			else
			{
				//Debug.Log("Evaluating: "+var+"... \t"+x+" != "+fa+" \n\t\t\t\t\t\t\t\t\t\t"+(x!=fa).ToString());
				return x != fa;
			}
		}

		public override string ToString ()
		{
			if (op == EvalOperator.IsTrue)
				return string.Format ("{0}=true", var);
			else if (op == EvalOperator.IsFalse)
				return string.Format ("{0}=false", var);
			else
				return string.Format ("{0}{1}{2}", var, EvalOpToText(op), a);
		}

		public static string EvalOpToText(EvalOperator op)
		{
			return op == EvalOperator.Equals ? "=" : op == EvalOperator.NotEquals ? "!=" : op == EvalOperator.LessThanEqual ? "<=" : op == EvalOperator.LessThan ? "<" : op == EvalOperator.GreaterThanEqual ? ">=" : op == EvalOperator.GreaterThan ? ">" : "";
		}

		public static EvalOperator TextToEvalOp(string txt)
		{
			return txt == "=" ? EvalOperator.Equals : txt == "!=" ? EvalOperator.NotEquals : txt == "<=" ? EvalOperator.LessThanEqual : txt == "<" ? EvalOperator.LessThan : txt == ">=" ? EvalOperator.GreaterThanEqual : txt == ">" ? EvalOperator.GreaterThan : txt == "!" ? EvalOperator.IsFalse : EvalOperator.IsTrue;
		}
	}

	public enum EvalOperator { Equals, NotEquals, LessThanEqual, LessThan, GreaterThanEqual, GreaterThan, IsTrue, IsFalse }
}