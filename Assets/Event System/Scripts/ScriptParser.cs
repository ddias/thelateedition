using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace EventMaster
{
	[ExecuteInEditMode()]
	public class ScriptParser {		
		private Dictionary<string,PhraseProcessor> rulePhrases = new Dictionary<string, PhraseProcessor>();
		private Dictionary<string,PhraseProcessor> responsePhrases = new Dictionary<string, PhraseProcessor>();
		
		private Dictionary<string,object> ruleVariables = new Dictionary<string, object>();
		private Dictionary<string,object> responseVariables = new Dictionary<string, object>();
		
		private BaseAddon[] addons;

		public static Dictionary<string,PhraseProcessor> RulePhrases = new Dictionary<string, PhraseProcessor>();
		public static Dictionary<string,PhraseProcessor> ResponsePhrases = new Dictionary<string, PhraseProcessor>();

		public ScriptParser(BaseAddon[] _addons) 
		{
			addons = _addons;
			rulePhrases.Clear();
			responsePhrases.Clear();
			EventDatabase.Instance.Rules.Clear();
			EventDatabase.Instance.Responses.Clear();
			
			// Supported rule phrases
			rulePhrases.Add("concept",GenericTagProcessor);
			rulePhrases.Add("target",GenericTagProcessor);
			rulePhrases.Add("criteria",CriteriaProcessor);
			rulePhrases.Add("response",GenericTagProcessor);
			// Load addon phrases
			foreach(BaseAddon addon in addons)
			{
				foreach(KeyValuePair<string,PhraseProcessor> v in addon.RulePhrases)
				{
					rulePhrases.Add(v.Key,v.Value);
				}
			}
			// Supported response phrases
			responsePhrases.Add("source",GenericTagProcessor);
			responsePhrases.Add("then",ThenProcessor);
			responsePhrases.Add("remember",VarDictionaryAppendProcessor);
			// Load addon phrases
			foreach(BaseAddon addon in addons)
			{
				foreach(KeyValuePair<string,PhraseProcessor> v in addon.ResponsePhrases)
				{
					responsePhrases.Add(v.Key,v.Value);
				}
			}
			RulePhrases = rulePhrases;
			ResponsePhrases = responsePhrases;
			ResetVariables();
		}
	
		public void ResetVariables()
		{
			ruleVariables.Clear();
			responseVariables.Clear();
			// Supported rule phrase variables
			ruleVariables.Add("concept","");
			ruleVariables.Add("target","any");
			ruleVariables.Add("criteria",null);
			ruleVariables.Add("response","");
			// Load addon variables
			foreach(BaseAddon addon in addons)
			{
				foreach(KeyValuePair<string,object> v in addon.RuleVariables)
				{
                    if (v.Value.GetType() == typeof(string))
                        ruleVariables.Add(v.Key, "");
                    else
                        ruleVariables.Add(v.Key,Activator.CreateInstance(v.Value.GetType()));
				}
			}
			// Supported response phrase variables
			responseVariables.Add("source","");
			responseVariables.Add("then",new Query());
			responseVariables.Add("remember",new Dictionary<string,float>());
			// Load addon variables
			foreach(BaseAddon addon in addons)
			{
				foreach(KeyValuePair<string,object> v in addon.ResponseVariables)
				{
                    if (v.Value.GetType() == typeof(string))
                        responseVariables.Add(v.Key, "");
                    else
                        responseVariables.Add(v.Key,Activator.CreateInstance(v.Value.GetType()));
				}
			}
		}

		public List<object> Parse(string file, string[] contents)
		{
			return Parse (file, contents, true);
		}

		public List<object> Parse(string file, string[] contents, bool clear)
		{
			// TODO: Move this somewhere else once dependencies are evaluated
			if (clear)
			{
				EventDatabase.Instance.Rules.Clear();
				EventDatabase.Instance.Responses.Clear();
			}
			List<object> blocks = new List<object>();
			Queue lines = new Queue(contents);
			bool processingRule = false;
			bool processingResponse = false;
			
			string blockName = "";
	
			while(lines.Count > 0)
			{
				string line = (string)lines.Dequeue();
				if (line == "{" || line.Length == 0) { continue; }
				line = line.TrimStart(new char[] {'\t',' '});
				string lcLine = line.ToLower();
				string keyword = line.Split(new char[] { ' ', '\t' })[0];
				if (!processingRule && !processingResponse)
				{
					if (lcLine.StartsWith("rule"))
					{
						processingRule = true;
						blockName = line.Split(' ')[1];
						continue;
					}
					if (lcLine.StartsWith("response"))
					{
						processingResponse = true;
						blockName = line.Split(' ')[1];
						continue;
					}
				}
				else if (processingRule)
				{
					if (lcLine.StartsWith("}"))
					{
						string concept = (string)ruleVariables["concept"];
						string target = (string)ruleVariables["target"];
						CriterionStatic[] criteria = (CriterionStatic[])ruleVariables["criteria"];
						string response = (string)ruleVariables["response"];
						Rule cachedRule = new Rule(blockName.ToLower(),concept,target,criteria,response);
						foreach(BaseAddon addon in addons)
						{
							addon.AfterRuleParsed(blockName.ToLower(),cachedRule);
						}
						blocks.Add(new Rule(blockName.ToLower(),concept,target,criteria,response));
						processingRule = false;
						blockName = "";
						ResetVariables();
					}
					else if (rulePhrases.ContainsKey(keyword))
					{
						line = line.Substring(line.IndexOf(keyword)+keyword.Length).Replace("\t","").TrimStart().TrimEnd();
						// TODO: Better way to alter the value in the collection?
						rulePhrases[keyword.ToLower()](keyword.ToLower(), line, ruleVariables);
					}
					else
					{
						Debug.LogWarning("Keyword in rule block ("+blockName+") is not supported:\n"+lcLine);
					}
				}
				else if (processingResponse)
				{
					if (lcLine.StartsWith("}"))
					{
						Query then = (Query)responseVariables["then"];
						string source = (string)responseVariables["source"];
						Dictionary<string,float> remember = (Dictionary<string,float>)responseVariables["remember"];
						Dictionary<string,object> addonStorage = new Dictionary<string, object>();
						foreach(BaseAddon addon in addons)
						{
							foreach(KeyValuePair<string,object> kvp in addon.ResponseVariables)
							{
								addonStorage.Add(kvp.Key,responseVariables[kvp.Key]);
							}
						}
						Response cachedResponse = new Response(blockName.ToLower(), then, source, remember, addonStorage);
						
						foreach(BaseAddon addon in addons)
						{
							addon.AfterResponseParsed(blockName.ToLower(),cachedResponse);
						}
						blocks.Add(cachedResponse);
						processingResponse = false;
						blockName = "";
						ResetVariables();
					}
					else if (responsePhrases.ContainsKey(keyword))
					{
						line = line.Substring(line.IndexOf(keyword)+keyword.Length).Replace("\t","").TrimStart().TrimEnd();
						// TODO: Better way to alter the value in the collection?
						responsePhrases[keyword.ToLower()](keyword.ToLower(), line, responseVariables);
					}
					else
					{
						Debug.LogWarning("Keyword in response block ("+blockName+") is not supported:\n"+lcLine);
					}
				}
			}
			return blocks;
		}
		
		public void HashRules(List<Rule> data, Dictionary<int,List<Rule>> target)
		{
			foreach(Rule r in data)
			{
				System.Text.StringBuilder sHash = new System.Text.StringBuilder();
				sHash.Append(r.Concept.ToLower());
				sHash.Append(r.Target.ToLower());
				foreach(BaseAddon addon in addons)
				{
					sHash.Append(addon.ExtendHash());
				}
				int hash = (sHash.ToString()).GetHashCode(); // For faster matching
				if (!target.ContainsKey(hash))
					target.Add(hash,new List<Rule>());
				
				if (target[hash].Find(rule=> {
					return (r.Name == rule.Name);
				}) != null)
					Debug.LogWarning("Duplicate rule: "+r.Name);
				Rule ruleCopy = new Rule(r);
				
				target[hash].Add(ruleCopy);
			}
		}
		
		public static CriterionStatic CreateComparison(string criteria)
		{
			// EVM-18 - Workaround for the workaround
			criteria = criteria.Replace("!=false","=true").Replace("!=true","=false");
			string[] comps = new string[] { ">=", "<=", "!=", "<", ">", "=" };
			foreach(string c in comps)
			{
				string[] var = criteria.Split(new string[] { c },StringSplitOptions.None);
				if (var.Length > 1)
				{
					if (!Memory.Instance.memory.ContainsKey(var[0]))
					{
						//Debug.Log ("Adding to memory: " + var[0]);
						Memory.Instance.memory.Add (var[0], 0);
					}
	
					double a = 0;
					var[1] = var[1].ToLower();
					EvalOperator op;
					if (var[1] != "true" && var[1] != "false")
					{
						a = Convert.ToDouble(var[1]);
						op = CriterionStatic.TextToEvalOp(c);
					} else {
						if (var[1] == "true")
							op = EvalOperator.IsTrue;
						else
							op = EvalOperator.IsFalse;
					}
					return new CriterionStatic(var[0], op, a);
				}
			}
			if (!Memory.Instance.memory.ContainsKey(criteria))
			{
				//Debug.Log ("Adding to memory: " + criteria);
				Memory.Instance.memory.Add (criteria, 0);
			}
			// True or False. Made it MaxValue to accomodate incremental "remember" keyword.
			// TODO: Consider handling a "set" and an "increment" separately in "remember"
			return new CriterionStatic(criteria,EvalOperator.IsTrue,1);
		}
		
		public static void CriteriaProcessor(string keyword, string value, Dictionary<string,object> variables)
		{
			string[] scriteria = value.Split(' ');
			CriterionStatic[] criteria = new CriterionStatic[scriteria.Length];
			for(int i=0;i<scriteria.Length;i++)
			{
				criteria[i] = CreateComparison(scriteria[i]);
			}
			variables[keyword] = criteria;
		}
		
		public static void ThenProcessor(string keyword, string value, Dictionary<string,object> variables)
		{
			string[] split = value.Split(' ');
			Query then = new Query();
			then.target = split[0];
			then.concept = split[1];
			variables[keyword] = then;
		}
		
		// Shareable processors
		public static void GenericTagProcessor(string keyword, string value, Dictionary<string,object> variables)
		{
			variables[keyword] = value.ToLower();
		}
		
		public static void StringListAppendProcessor(string keyword, string value, Dictionary<string,object> variables)
		{
			List<string> list = (List<string>)variables[keyword];
			list.Add(value);
			variables[keyword] = list;
		}
		
		public static void VarDictionaryAppendProcessor(string keyword, string value, Dictionary<string,object> variables)
		{
			Dictionary<string,float> d = (Dictionary<string,float>)variables[keyword];
			string[] split = value.Split(' ');
			for(int i = 0; i < split.Length; i++)
			{
				string[] var = split[i].Split(':');
				if (var.Length != 2)
				{
					Debug.LogError("Script Parser error in VarDictionaryAppendProcessor!\n keyword: "+keyword+", value: "+value);
				}
				var[1] = var[1].Replace("true","1").Replace("false","0");
				d.Add(var[0], Convert.ToSingle(var[1]));
			}
			variables[keyword] = d;
		}
		
		public delegate void PhraseProcessor(string keyword, string value, Dictionary<string,object> variables);
	}
}