﻿using System.Collections.Generic;

namespace EventMaster
{
	public class Symbolic {
		
		private static Symbolic instance;
		public static Symbolic Instance { get { if (instance == null) {  instance = new Symbolic(); } return instance; } }
		
		private float vali = 0;
		
		public Dictionary<string,float> Vars { get; internal set;}
		
		public Symbolic()
		{
			Vars = new Dictionary<string, float>();
		}
		
		public void Add(string str)
		{
			Vars.Add(str,vali++);
		}
	}
}