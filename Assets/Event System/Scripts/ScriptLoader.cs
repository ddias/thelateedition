using UnityEngine;
using System.Collections.Generic;
using System;
using System.IO;
using System.Reflection;

namespace EventMaster
{
	[ExecuteInEditMode()]
	public class ScriptLoader{
		// TODO: At some point localization should take care of which folder to load. For now, set it manually
		public string locale = "en";
	
		// TODO: Change this into
		private string[] tempFiles = new string[] { "main" };
	
		// Use this for initialization
		public void LoadAll () {
			BaseAddon[] addons = GetAddons();
			ScriptParser parser = new ScriptParser(addons);
			// First parse built in data
			string path = "Scripts/"+locale+"/";
			List<Rule> rules = new List<Rule>();
			List<Response> responses = new List<Response>();
			foreach (string f in tempFiles) 
			{
				List<object> blocks;
				UnityEngine.TextAsset file = (UnityEngine.TextAsset)UnityEngine.Resources.Load(path+f,typeof(UnityEngine.TextAsset));
				blocks = parser.Parse(f,file.text.Replace("\r\n","\n").Split('\n'));
				foreach(object block in blocks)
				{
					if (block is Rule)
					{
						rules.Add(new Rule((Rule)block));
					}
					else if (block is Response)
					{
						responses.Add(new Response((Response)block));
					}
				}
			}
			// Attempt to override with stuff in dataPath
			/*try
			{
				path = Application.dataPath + "/Resources/Scripts/" + locale + "/";
				foreach(string f in Directory.GetFiles(path))
				{
					if (f.EndsWith("txt"))
					{
						//				Debug.Log ("Parsing " + f);
						List<object> blocks;
						blocks = parser.Parse(f.Substring(f.LastIndexOf("/")+1).Replace(".txt",""),File.ReadAllLines(f));
						foreach(object block in blocks)
						{
							if (block is Rule)
							{
								rules.Add(new Rule((Rule)block));
							}
							else if (block is Response)
							{
								responses.Add(new Response((Response)block));
							}
						}
					}
				}
			}
			catch(System.Exception ex)
			{
				// Let's ignore that since these are merely overrides
			}*/
			parser.HashRules(rules,EventDatabase.Instance.Rules);
			foreach(Response res in responses)
			{
                //Debug.LogFormat("added key {0}", res.name);
				EventDatabase.Instance.Responses.Add(res.name,res);
			}
			// Sort the rules by number of criteria
			foreach(KeyValuePair<int, List<Rule>> kvp in EventDatabase.Instance.Rules)
			{
				System.Comparison<Rule> comp = new System.Comparison<Rule>(Rule.CompareCount);
				kvp.Value.Sort (comp);
			}
	
			//sanity check, make sure we dont have typos in rule/response names
			SanityCheck();
		}

		public void Load(string name, BaseAddon[] addons) {
			// Sanitize string
			// We expect users will be passing in the scene name... so let's remove the .unity
			name = name.Replace(".unity","");
			// We don't want path information... at least so far...
			name = name.Substring(name.LastIndexOf('/')+1,name.Length - (name.LastIndexOf('/')+1));
			// First parse built in data
			string path = "Scripts/" + locale + "/";
			ScriptParser parser = new ScriptParser(addons);
			UnityEngine.TextAsset file = (UnityEngine.TextAsset)UnityEngine.Resources.Load(path+name,typeof(UnityEngine.TextAsset));
			try
			{
				parser.Parse(name,file.text.Replace("\r\n","\n").Split('\n'));
			}
			catch(System.Exception ex)
			{
                
			}
			// Attempt to override with stuff in dataPath
			if (Application.isPlaying)
			{
				try
				{
					path = Application.dataPath + "/Scripts/"+path;
					parser.Parse(name,File.ReadAllLines(path+name+".txt"));
				}
				catch(System.Exception ex)
				{
					// Let's ignore that since these are merely overrides
				}
			}
			
			// Sort the rules by number of criteria
			foreach(KeyValuePair<int, List<Rule>> kvp in EventDatabase.Instance.Rules)
			{
				System.Comparison<Rule> comp = new System.Comparison<Rule>(Rule.CompareCount);
				kvp.Value.Sort (comp);
				//Debug.Log (kvp.Key);
			}
			
			//sanity check, make sure we dont have typos in rule/response names
			SanityCheck();
		}
		
		public BaseAddon[] GetAddons()
		{
			List<BaseAddon> addons = new List<BaseAddon>();
			Assembly assembly = Assembly.GetAssembly (typeof(BaseAddon));
			foreach (Type t in assembly.GetTypes()) 
			{
				if (t.BaseType == typeof(BaseAddon))
				{
					addons.Add((BaseAddon)Activator.CreateInstance(t));
				}
			}
			return addons.ToArray();
		}

	
		public void SanityCheck()
		{
			foreach(KeyValuePair<int, List<Rule>> kvp in EventDatabase.Instance.Rules)
			{
				foreach(Rule r in kvp.Value)
				{
					if(!EventDatabase.Instance.Responses.ContainsKey(r.Response))
						Debug.LogWarning ("WARNING! - Rule '" + r.Name + "' contains a reference to a response that doesnt exist");
					if(r.Criteria.Length < 1 && EventSettings.Instance.SupressCriteriaWarnings)
						Debug.LogWarning ("WARNING - Rule '" + r.Name + "' doesn't have any criteria");
				}
			}
		}
	}
}