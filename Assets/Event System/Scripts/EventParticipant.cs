﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace EventMaster
{
	[ExecuteInEditMode()]
	[System.Serializable()]
	public class EventParticipant : MonoBehaviour {
        [HeaderAttribute("EventMaster")]
        public string id = "";
		public List<Rule> Rules;

        private static Dictionary<string, GameObject> lookup = new Dictionary<string, GameObject>();
        protected GameObject controller;

        public GameObject Lookup(string key) 
		{
			return lookup[key];
		}
		
		public static string[] Participants { 
			get {
				string[] p = new string[lookup.Keys.Count];
				lookup.Keys.CopyTo(p,0);
				return p;
			} 
		}

        public void PerformQuery(string concept)
        {
            if (controller == null)
                controller = GameObject.Find("Controller");
            ExecuteEvents.Execute<IEventQueryHandler>(controller, null, (x, y) => x.QueryAndResolve(new Query(concept,id)));
        }
	
		// Use this for initialization
		void Start () {
			if (!Application.isPlaying)
			{
				if (id.Length == 0)
					id = gameObject.name.ToLower();
			}
			else {

				if(!EventParticipant.lookup.ContainsKey(id.ToLower ()))
					EventParticipant.lookup.Add(id.ToLower(),this.gameObject);
			}
		}

		void Update() {
			if (!Application.isPlaying)
			{
				if(!EventParticipant.lookup.ContainsKey(id.ToLower ()))
					EventParticipant.lookup.Add(id.ToLower(),this.gameObject);
			}
		}
	}
}