﻿using UnityEngine;
using System.Collections;

namespace EventMaster
{
	[ExecuteInEditMode()]
	public sealed class EventSettings 
	{
		private static readonly EventSettings instance = new EventSettings();
		
		static EventSettings() { }
		private EventSettings() { }
		
		public static EventSettings Instance { get { return instance; } }
		
		public bool SupressCriteriaWarnings = false;
	}
}