﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;

namespace EventMaster
{
	[ExecuteInEditMode()]
	public sealed class EventDatabase 
	{
		private static readonly EventDatabase instance = new EventDatabase();
		
		static EventDatabase() { }
		private EventDatabase() { }
		
		public static EventDatabase Instance
		{
			get
			{
				return instance;
			}
		}
		public Dictionary<int,List<Rule>> Rules { get { return rules; } }
		public Dictionary<string,Response> Responses { get { return responses; } }
		private Dictionary<int,List<Rule>> rules = new Dictionary<int, List<Rule>>();
		private Dictionary<string,Response> responses = new Dictionary<string,Response>();

		public void Clear()
		{
			rules.Clear();
			//responses.Clear();
		}
	
		public void SaveDB()
		{
            /*
			// TODO: Save what file rules are in? I don't know.
			string filePath = Application.dataPath + "/Resources/Scripts/en/main.txt";
			if (File.Exists (filePath)) { File.Delete(filePath); }
			else { File.CreateText(filePath).Close(); }
			TextWriter file = File.CreateText(filePath);
			foreach(KeyValuePair<int,List<Rule>> kvp in rules)
			{
				foreach(Rule r in kvp.Value)
				{
					file.Write(r.ToString());
					// TODO: Track which responses are written when implementing linked responses
					// TODO: Maybe store the name inside the Response class...? Then we could write the whole thing.
					if (r.Response != "")
					{
						file.Write("Response {0}{1}{{{1}",r.Response,Environment.NewLine);
						file.Write(responses[r.Response].ToString());
						file.Write("}}{0}",Environment.NewLine);
					}
				}
			}
			file.Close();
            */
		}

        public Rule CompareQuery(Query q)
		{
			Result finalResult = new Result();
			finalResult.match = -1;
			
			int hashedQuery = (q.concept.ToLower () + q.target.ToLower ()).GetHashCode();
			List<Rule> matchedRules = new List<Rule>();
			
			// Get the matched hash rules
			if(rules.ContainsKey(hashedQuery))
				matchedRules = (rules[hashedQuery]);
			
			// Check if there are any rules that have a broader target
			else 
			{
				hashedQuery = (q.concept + "any").GetHashCode();
				if(rules.ContainsKey(hashedQuery))
					matchedRules = (rules[hashedQuery]);
			}
			
			
			// If there are matching rules lets start comparing them
			//Debug.Log ("Number of possible matches: " + matchedRules.Count);
			if (matchedRules.Count != 0)
			{
				foreach(Rule r in matchedRules)
				{
					//Debug.Log ("Comparing rule : " + r.Name);
					int match = 0;
					bool denied = false;
					//Debug.Log ("FinalResult count = " + finalResult.match);
					//Debug.Log ("Current rule count = " + r.Criteria.Length);
					
					// if the match of the result is bigger than the count of criteria thats left... no need to check anymore rules
					if (finalResult.match > r.Criteria.Length)
					{
						//Debug.Log ("Lets get outta here, match was found and the criteria for the next rules are small");
						break;
					}
					
					for(int i = 0; i < r.Criteria.Length; i++)
					{
						if(Memory.Instance.memory.ContainsKey(r.Criteria[i].var))
						{
							// Checking to see if it matches the criteria
							if (r.Criteria[i].Compare(Memory.Instance.memory[r.Criteria[i].var]))
							{
								//Debug.Log (Memory.Instance.memory[r.Criteria[i].var] + " comparing to " + r.Criteria[i].var);
								match++;
							}
							// the criteria and memory dont match
							else
							{
								denied = true;
								break;
							}
						}
						// there is a criteria that doesnt exist in the world so break
						else
						{
							denied = true;
							break;
						}
					}
					
					// As long as the rule criterias all existed in the query
					if(!denied)
					{
						Result result = new Result(r, match);
						//Debug.Log ("Result found: " + result.r.Name);
						if(finalResult.match == result.match)
						{
							System.Random rand = new System.Random();
							int roll = rand.Next (0, 100);
							if(roll < 50)
								finalResult = result;
						}
						
						// if its a better match (matched more rules) now its the result
						else if(finalResult.match < result.match)
						{
							finalResult = result;
						}
					}
				}
				
			}
			
			// If we didn't find a match return null
			if(finalResult.match == -1)
			{
				//Debug.Log ("no hash found");
				return null;
			}
			
			return finalResult.r;
		}
	}
}