﻿using UnityEngine;
using System;
using System.Text;
using System.Collections.Generic;
using System.Reflection;

namespace EventMaster
{
	[System.Serializable]
	public class Response {
	
		public string name { get; set; }
		public Query then { get; set; }
		public string source { get; set; }
		public Dictionary<string, float> remember { get; set; }
		public Dictionary<string, object> addonStorage { get; set; }

		public Response(Response r)
		{
			name = r.name;
			then = r.then;
			source = r.source;
			remember = new Dictionary<string, float>();
			foreach (KeyValuePair<string,float> kvp in r.remember)
				remember.Add(kvp.Key,kvp.Value);
			addonStorage = new Dictionary<string, object>();
			foreach(KeyValuePair<string,object> kvp in r.addonStorage)
				addonStorage.Add(kvp.Key,kvp.Value);
		}

		public Response(string _name, Query _then, string _source, Dictionary<string, float> _remember, 
		                Dictionary<string, object> _addonStorage)
		{
			name = _name;
			then = _then;
			source = _source;
			remember = _remember;
			addonStorage = _addonStorage;
		}

		public override string ToString ()
		{
			StringBuilder sb = new StringBuilder();
			// TODO: Maybe have the name inside this class? And write the whole thing in this method.
			if (source.Length > 0) { sb.AppendFormat("\tsource {0}{1}",source,Environment.NewLine); }

			foreach(KeyValuePair<string,object> store in addonStorage)
			{
				if (store.Value is System.Collections.IList)
				{
					System.Collections.IList list = (store.Value as System.Collections.IList);
					foreach(object obj in list)
					{
						sb.AppendFormat("\t{0} {1}{2}",store.Key,obj.ToString(),Environment.NewLine);
					}
				}
				else
				{
					sb.AppendFormat("\t{0} {1}{2}",store.Key,store.Value.ToString(),Environment.NewLine);
				}
			}

			foreach(KeyValuePair<string,float> rem in remember)
			{
				sb.AppendFormat("\tremember {0}:{1}{2}",rem.Key,rem.Value,Environment.NewLine);
			}
			if (then != null)
			{
				if (then.target.Length > 0 && then.concept.Length > 0)
				{
					sb.AppendFormat("\tthen {0}{1}",then.ToString(),Environment.NewLine); 
				}
			}
			return sb.ToString();
		}
	};
	
	[System.Serializable]
	public class Query 
	{
		public string target;
		public string concept;
	
		public Query()
		{
			target = "";
			concept = "";
		}
		
		public Query(string _concept, string _target)
		{
			target = _target;
			concept = _concept;
		}

		public override string ToString ()
		{
			return string.Format ("{0} {1}", target, concept);
		}
	};
}