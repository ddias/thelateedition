﻿using System;
using System.Text;
using UnityEngine;
using System.Collections.Generic;

namespace EventMaster
{
	[System.Serializable]
	public class Rule {
		public string Name;
		public string Concept;
		public string Target;
		public CriterionStatic[] Criteria;
		public string Response;

		public Rule(Rule r)
		{
			Name = r.Name;
			Concept = r.Concept;
			Target = r.Target;
			Criteria = r.Criteria;
			Response = r.Response;
		}
		
		public Rule(string _name, string _concept, string _target, CriterionStatic[] _criteria, string _response)
		{
			Name = _name;
			Concept = _concept.ToLower();
			Target = _target.ToLower();
			if (_criteria == null)
				Criteria = new CriterionStatic[0];
			else
				Criteria = _criteria;
			Response = _response;
		}
	
		// reverse sort on criteria length comparison
		public static int CompareCount(Rule r1, Rule r2)
		{
			return r2.Criteria.Length.CompareTo(r1.Criteria.Length);
		}

		public override string ToString ()
		{
			StringBuilder sb = new StringBuilder ();
			sb.AppendFormat ("Rule {0}{1}{{{1}",Name,Environment.NewLine);
			if (Concept.Length > 0) { sb.AppendFormat ("\tconcept {0}{1}", Concept,Environment.NewLine); }
			if (Criteria != null && Criteria.Length > 0)
			{ 
				sb.Append("\tcriteria ");
				foreach(CriterionStatic cs in Criteria)
				{
					sb.AppendFormat("{0} ", cs.ToString());
				}
				sb.AppendFormat("{0}",Environment.NewLine);
			}
			if (Target.Length > 0) { sb.AppendFormat ("\ttarget {0}{1}", Target,Environment.NewLine); }
			if (Response.Length > 0) { sb.AppendFormat ("\tresponse {0}{1}",Response,Environment.NewLine); }
			sb.AppendFormat("}}{0}",Environment.NewLine);
			return sb.ToString ();
		}
	}
}