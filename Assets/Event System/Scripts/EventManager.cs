﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;

namespace EventMaster
{	
	[ExecuteInEditMode()]
	public class EventManager : MonoBehaviour, IEventQueryHandler {
		
		BaseAddon[] addons;
		
		ScriptLoader sl = new ScriptLoader();
		
		public bool WatchFiles = true;
	
		// Use this for initialization
		public void Start () {
			// TODO: Re-eval scene level loading of event scripts
			//addons = GetComponents<BaseAddon>();
			if (Application.isPlaying)
				sl.LoadAll();//(Application.loadedLevelName, addons);
			else
				sl.LoadAll();//(UnityEditor.EditorApplication.currentScene, addons);
			addons = sl.GetAddons();
			// Write addon details to json file for editor
			//sl.WriteAddonFile();
			
            /*
			if(WatchFiles)
			{
				Debug.Log("Now watching some files in " + Application.dataPath + "/Resources/Scripts/en/");
				FileWatcher fw = new FileWatcher(Application.dataPath + "/Resources/Scripts/en/", 10);
				fw.startWatching();
			}
            */
		}
	
		public bool QueryAndResolve(Query q)
		{
            Debug.Log(q.target + ": " + q.concept);
			return ResolveResult(EventDatabase.Instance.CompareQuery(q));
		}
	
		public bool ResolveResult(Rule matchedRule)
		{
			if(matchedRule != null)
			{
                Debug.Log("Matched Rule: " + matchedRule.Name);
                string responseName = matchedRule.Response;
				Response res = EventDatabase.Instance.Responses[responseName];
                foreach (BaseAddon addon in addons)
				{
					addon.ResolveResponse(res);
				}
				Remember(res.remember);
				return true;
			}
			else
				return false;
		}
	
		void Remember(Dictionary<string, float> _remember)
		{
			foreach(KeyValuePair<string, float> kvp in _remember)
			{
				if(Memory.Instance.memory.ContainsKey(kvp.Key))
					Memory.Instance.memory[kvp.Key] += kvp.Value;
				else
					Memory.Instance.memory.Add(kvp.Key, kvp.Value);
				//Debug.Log ("Increased memory of '" + kvp.Key + "' by " + kvp.Value);
				//Debug.Log (Memory.Instance.memory[kvp.Key]);
			}
		}
	};
	
	public struct Result
	{
		public Rule r;
		public int match;
		
		public Result(Rule _r, int _match)
		{
			r = _r;
			match = _match;
		}
	};
    public interface IEventQueryHandler : IEventSystemHandler
    {
        bool QueryAndResolve(Query q);
    }
}