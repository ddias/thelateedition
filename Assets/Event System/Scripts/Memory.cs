﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace EventMaster
{
	public sealed class Memory {
	
		private static readonly Memory instance = new Memory();
	
		private string defaultDirectoryPath = Application.dataPath+"/Resources/Saves/";
		private string defaultFileName = "world.txt";
	
		static Memory() { }
		private Memory() { }
		
		public static Memory Instance
		{
			get
			{
				return instance;
			}
		}
		public Dictionary<string,float> memory = new Dictionary<string, float>();
	
		public void SaveMemory()
		{
			SaveMemory(defaultDirectoryPath, defaultFileName);
		}

		public void SaveMemory(string path, string file)
		{
			List<string> temp = new List<string>();
			foreach (KeyValuePair<string, float> kvp in memory)
			{
				temp.Add(kvp.Key + " " + kvp.Value);
			}
			// TODO: Fix saving memory
			string[] test = temp.ToArray();
			if(!Directory.Exists(path))
			{
				Debug.Log ("doesnt exist");
				Directory.CreateDirectory (path);
			}
			//File.WriteAllLines(path + file, test); // Errors out in the web build. Consider revisiting.
			Debug.Log ("Wrote file out to " + path+file);
		}

		public void LoadMemory()
		{
			LoadMemory(defaultDirectoryPath+defaultFileName);
		}
	
		public void LoadMemory(string path)
		{
			if (File.Exists(path))
			{
				string[] readIn = File.ReadAllLines(path);
				Queue lines = new Queue(readIn);
				while (lines.Count > 0)
				{
					string line = (string)lines.Dequeue();
					string[] var = line.Split(' ');
					if(memory.ContainsKey (var[0]))
						memory[var[0]] = (float)Convert.ToDouble(var[1]);
					else
						memory.Add(var[0], (float)Convert.ToDouble(var[1]));
				}
			}
			else
			{
				Debug.Log ("File does not exist");
			}
		}
	
		public void PrintMemory()
		{
			foreach(KeyValuePair<string, float> kvp in memory)
			{
				Debug.Log (kvp.Key + " : " + kvp.Value);
			}
		}
	}
}