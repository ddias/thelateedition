﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;

namespace EventMaster
{
	[ExecuteInEditMode()]
	public sealed class EventEditorDatabase {
		public SortedDictionary<string,Rule> Rules { get; internal set; }
		public SortedDictionary<string,Response> Responses { get; internal set; }
		public List<string> EventVariables  { get; internal set; }
	
		private static readonly EventEditorDatabase instance = new EventEditorDatabase();
		static EventEditorDatabase() { }
		private EventEditorDatabase() {
			EventVariables = new List<string>();
			Rules = new SortedDictionary<string, Rule> ();
			Responses = new SortedDictionary<string, Response>();
		}
		
		public void Reload()
		{
			Rules.Clear();
			Responses.Clear();
			EventVariables.Clear();
			// TODO: Not sure if we should do this here... but we need to parse  the scripts at some point.
			ScriptLoader sl = new ScriptLoader();
			sl.LoadAll();
			foreach (KeyValuePair<int, List<Rule>> kvp_rules in EventDatabase.Instance.Rules) 
			{
				foreach(Rule rule in kvp_rules.Value)
				{
					Rules.Add(rule.Name,rule);
				}
			}
			foreach (KeyValuePair<string,Response> kvp_response in EventDatabase.Instance.Responses) 
			{
				Responses.Add(kvp_response.Key,kvp_response.Value);
			}
		}

        public string GenerateBase64()
        {
            ScriptLoader sl = new ScriptLoader();
            sl.LoadAll();
            WriteAddonFile(sl);
            string sScript = File.ReadAllText(String.Format("{0}/Resources/Scripts/en/main.txt", Application.dataPath), System.Text.Encoding.UTF8);
            string results = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(sScript));
            return results;
        }

        public void WriteAddonFile(ScriptLoader sl)
        {
            BaseAddon[] addons = sl.GetAddons();

            if (addons.Length > 0)
            {
                List<string> lines = new List<string>();
                lines.Add("var addonData = [");
                for (int i = 0; i < addons.Length; i++)
                {
                    BaseAddon addon = addons[i];
                    int j = 0;
                    foreach (KeyValuePair<string, ScriptParser.PhraseProcessor> response in addon.ResponsePhrases)
                    {
                        lines.Add("\t{");
                        string keyword = response.Key;
                        lines.Add(String.Format("\t\t'keyword': '{0}',", keyword));
                        lines.Add(String.Format("\t\t'helper': '{0}'", addon.Helper[keyword]));
                        if (addon.ResponseVariables[keyword].GetType().FullName.Contains("List"))
                        {
                            lines.Add("\t\t,'multiple': 'true'");
                        }
                        j++;
                        if (i == addons.Length - 1 && j == addon.ResponsePhrases.Count)
                            lines.Add("\t}");
                        else
                            lines.Add("\t},");
                    }
                }
                lines.Add("];");
                File.WriteAllLines(Application.dataPath + "/Event System/.EventMasterWebEditor/js/addons.js", lines.ToArray(), System.Text.Encoding.UTF8);
            }
        }

        public static EventEditorDatabase Instance
		{
			get
			{
				return instance;
			}
		}
	}
}