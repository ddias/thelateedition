﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

namespace EventMaster
{
	public static class EditorHelper 
	{
		public static void DisplayList<T>(List<T> list) where T : new()
		{
			DisplayList(list, "List of " + typeof(T).ToString());
		}

		public static void DisplayList<T>(List<T> list, string title) where T : new()
		{
			T[] items = new T[list.Count];
			items = list.ToArray();
			
			EditorGUILayout.Space();
			EditorGUILayout.LabelField(title);
			if(GUILayout.Button ("Add", GUILayout.Width (150)))
			{
				bool notInserted = true;
				int count = 0;
				while(notInserted)
				{
					count++;
					list.Add(new T());
					ArrayUtility.Clear(ref items);
					foreach(T item in list)
					{
						ArrayUtility.Add(ref items,item);
					}
					notInserted = false;
				}
			}
			int removed = -1;
			for(int counter=0;counter<items.Length;counter++)
			{
				EditorGUILayout.BeginHorizontal ();
				if(GUILayout.Button ("-", GUILayout.Width (30)))
					removed = counter;
				if (typeof(T) == typeof(string))
					items[counter] = (T)Convert.ChangeType(EditorGUILayout.TextField("", Convert.ToString(items[counter]),GUILayout.Width (180)),typeof(T));
				if (typeof(T) == typeof(float))
					items[counter] = (T)Convert.ChangeType(EditorGUILayout.FloatField("", Convert.ToSingle(items[counter]),GUILayout.Width (180)),typeof(T));
				if (typeof(T) == typeof(int))
					items[counter] = (T)Convert.ChangeType(EditorGUILayout.IntField("", Convert.ToInt32(items[counter]),GUILayout.Width (180)),typeof(T));
				EditorGUILayout.EndHorizontal ();
			}
			if(removed != -1)
			{
				list.RemoveAt(removed);
				ArrayUtility.Clear(ref items);
				foreach(T item in list)
				{
					ArrayUtility.Add(ref items,item);
				}
			}
		}

		public static void DisplayDictionary<V>(Dictionary<string,V> collection) where V : new()
		{
			DisplayDictionary(collection, typeof(V).ToString() + " Collection");
		}

		public static void DisplayDictionary<V>(Dictionary<string,V> collection, string title) where V : new()
		{
			string[] customKeys = new string[collection.Keys.Count];
			V[] customValues = new V[collection.Values.Count];
			collection.Keys.CopyTo(customKeys, 0);
			collection.Values.CopyTo(customValues, 0);
			
			EditorGUILayout.Space();
			EditorGUILayout.LabelField(title);
			if(GUILayout.Button ("Add", GUILayout.Width (150)))
			{
				bool notInserted = true;
				int count = 0;
				while(notInserted)
				{
					count++;
					if(!collection.ContainsKey("Item" + count))
					{
						collection.Add("Item" + count, new V());
						ArrayUtility.Clear(ref customKeys);
						ArrayUtility.Clear(ref customValues);
						foreach(KeyValuePair<string, V> kvp in collection)
						{
							ArrayUtility.Add(ref customKeys,kvp.Key);
							ArrayUtility.Add(ref customValues, kvp.Value);
						}
						notInserted = false;
					}
				}
			}
			string removed = null;
			int counter = 0;
			foreach(string s in customKeys)
			{
				EditorGUILayout.BeginHorizontal ();
				if(GUILayout.Button ("-", GUILayout.Width (30)))
					removed = s;
				customKeys[counter] = EditorGUILayout.TextField ("", customKeys[counter],GUILayout.Width (180));
				if (typeof(V) == typeof(string))
					customValues[counter] = (V)Convert.ChangeType(EditorGUILayout.TextField("", Convert.ToString(customValues[counter]),GUILayout.Width (180)),typeof(V));
				if (typeof(V) == typeof(float))
					customValues[counter] = (V)Convert.ChangeType(EditorGUILayout.FloatField("", Convert.ToSingle(customValues[counter]),GUILayout.Width (180)),typeof(V));
				if (typeof(V) == typeof(int))
					customValues[counter] = (V)Convert.ChangeType(EditorGUILayout.IntField("", Convert.ToInt32(customValues[counter]),GUILayout.Width (180)),typeof(V));
				EditorGUILayout.EndHorizontal ();
				if (customKeys[counter] != s)
				{
					collection.Remove(s);
					collection.Add(customKeys[counter],customValues[counter]);
				}
				collection[customKeys[counter]] = customValues[counter];
				counter++;
			}
			if(removed != null)
			{
				if(collection.ContainsKey(removed))
				{
					collection.Remove(removed);
					ArrayUtility.Clear(ref customKeys);
					ArrayUtility.Clear(ref customValues);
					foreach(KeyValuePair<string, V> kvp in collection)
					{
						ArrayUtility.Add(ref customKeys,kvp.Key);
						ArrayUtility.Add(ref customValues,kvp.Value);
					}
				}
			}
		}
	}
}