﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace EventMaster
{
	[CustomEditor(typeof(EventManager))]
	public class EventManagerEditor : Editor {
	
		public override void OnInspectorGUI ()
		{
			EventSettings.Instance.SupressCriteriaWarnings = GUILayout.Toggle(
				EventSettings.Instance.SupressCriteriaWarnings, "Supress Empty Critiera Warnings?");
			base.OnInspectorGUI ();
		}
	}
}