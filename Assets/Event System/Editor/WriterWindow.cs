using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;

namespace EventMaster
{
	public class WriterWindow : EditorWindow {

		
		// Add menu named "My Window" to the Window menu
		[MenuItem ("Window/EventMaster/Editor")]
		public static void ShowEditor () 
		{
			ScriptLoader sl = new ScriptLoader();
			sl.LoadAll();
			string obj = EventEditorDatabase.Instance.GenerateBase64();
			//HACK: For some reason Unity isn't launching with parameters, so we'll have to create a temporary HTML file that launches it properly.
			string url = String.Format("file://{0}/Event System/.EventMasterWebEditor/index.html?script={1}",Application.dataPath,obj);
			string html = String.Format("<html><head><script>window.onload = function() {{window.location = '{0}';}}</script></head></html>",url);
			System.IO.File.WriteAllText(String.Format("{0}/Event System/.EventMasterWebEditor/launch.html",Application.dataPath),html);
			System.Diagnostics.Process.Start(String.Format("file://{0}/Event System/.EventMasterWebEditor/launch.html",Application.dataPath));
		}
	}
}