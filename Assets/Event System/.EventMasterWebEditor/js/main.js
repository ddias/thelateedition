var app = angular.module("evmoe",[]);

app.controller('main', function($scope) {
		$scope.addons = [];
		/* Rule Related Functions */
		$scope.SelectRule = function(id) {
			if ($scope.entryform.$invalid) 
			{
				alert('Please correct the errors outlined in red on the form.');
				return; 
			}
			$scope.selected = id;
			$scope.RefreshTypeAheads();
		};
		$scope.AddRule = function() {
			var id = $scope.rules.push({id: guid(), name:'',concept:'',criteria:$scope.NewCriteriaObj(),target:'',response:{id:guid(),source:'',remember:$scope.NewMemoryObj(),addonStorage:{},then:{target:'',concept:''}}});
			$scope.RefreshTypeAheads();
			return id;
		};
		$scope.NewCriteriaObj = function() {
			return [{id:guid(),name:'',op:'',value:''}];
		}
		$scope.NewMemoryObj = function() {
			return [{id: guid(),name: '',value:''}];
		}
		$scope.RemoveRule = function(rule) {
			var idx = $scope.rules.indexOf(rule);
			$scope.rules.splice(idx,1);
			if ($scope.rules.length == 0)
			{
				$scope.AddRule();
			}
			$scope.RefreshTypeAheads();
		};
		/* Criteria Related Functions */
		$scope.showCriteriaLabel = function(rule_id, criteria) {
			if (criteria == null) return true;
			return criteria.id === $scope.rules[rule_id].criteria[0].id;
		};
		$scope.showCriteriaAdd = function(rule_id, criteria) {
			if (criteria == null) return true;
			return criteria.id === $scope.rules[rule_id].criteria[$scope.rules[rule_id].criteria.length-1].id;
		};
		$scope.AddCriteria = function(rule_id) {
			if ($scope.rules[rule_id].criteria == null) { $scope.rules[rule_id].criteria = $scope.NewCriteriaObj(); }
			$scope.rules[rule_id].criteria.push({id:guid(),name:'',op:'',value:''});
			$scope.RefreshTypeAheads();
		};
		$scope.RemoveCriteria = function(rule_id,criteria) {
			var idx = $scope.rules[rule_id].criteria.indexOf(criteria);
			$scope.rules[rule_id].criteria.splice(idx,1);
			$scope.RefreshTypeAheads();
		};
		/* Memory Related Functions */
		$scope.showMemoryLabel = function(rule_id, memory) {
			if (memory == null) return true;
			return memory.id === $scope.rules[rule_id].response.remember[0].id;
		};
		$scope.showMemoryAdd = function(rule_id, memory) {
			if (memory == null) return true;
			return memory.id === $scope.rules[rule_id].response.remember[$scope.rules[rule_id].response.remember.length-1].id;
		};
		$scope.AddMemory = function(rule_id) {
			if ($scope.rules[rule_id].response.remember == null) { $scope.rules[rule_id].response.remember = $scope.NewMemoryObj(); }
			$scope.rules[rule_id].response.remember.push({id:guid(),name:'',value:''});
			$scope.RefreshTypeAheads();
		};
		$scope.RemoveMemory = function(rule_id,memory) {
			var idx = $scope.rules[rule_id].response.remember.indexOf(memory);
			$scope.rules[rule_id].response.remember.splice(idx,1);
			$scope.RefreshTypeAheads();
		};
		/* Addon Related Functions */
        $scope.showAddonValueAdd = function(rule_id,keyword,value) {
            if (value == null) return true;
            return value.id === $scope.rules[rule_id].response.addonStorage[keyword][$scope.rules[rule_id].response.addonStorage[keyword].length-1].id;
        };
        $scope.AddAddonValue = function(rule_id,keyword) {
            if ($scope.rules[rule_id].response.addonStorage[keyword] == null) { $scope.rules[rule_id].response.addonStorage[keyword] = []; }
            $scope.rules[rule_id].response.addonStorage[keyword].push({id:guid(),value:''});
        };
        $scope.RemoveAddonValue = function(rule_id,keyword,value) {
            var idx = $scope.rules[rule_id].response.addonStorage[keyword].indexOf(value);
            $scope.rules[rule_id].response.addonStorage[keyword].splice(idx,1);
        };
		/* Type Ahead */
		$scope.concepts = [];
		$scope.showTAConcepts = false;
		$scope.targets = [];
		$scope.showTATargets = false;
		$scope.showTASources = false;
		$scope.variables = [];
		$scope.criteriaTAShowing = -1;
		$scope.rememberTAShowing = -1;
		
		$scope.taLeft = 0;
		$scope.taTop = 0;
		$scope.taWidth = 0;
		
		$scope.ShowTypeAhead = function(event,index) {
			var offset = {x : 0, y : 0};
			GetOffset (event.target, offset);

			var scrolled = {x : 0, y : 0};
			GetScrolled (event.target.parentNode, scrolled);
			
			if (index != null)
			{
				if (event.target.className.indexOf('criteria') > -1)
				{
					$scope.criteriaTAShowing = index;
				}
				else if (event.target.className.indexOf('remember') > -1)
				{
					$scope.rememberTAShowing = index;
				}
			}

			var posX = offset.x - scrolled.x;
			var posY = offset.y - scrolled.y;
			
			$scope.taTop =  posY+event.target.offsetHeight;
			$scope.taLeft = posX;
			$scope.taWidth = event.target.offsetWidth;
		};
		
		$scope.HideTypeAhead = function(event,index) {
			// when the relatedTarget is not null, we're clicking on another field. So hide all type aheads.
			if (event.relatedTarget != null)
			{
				$scope.criteriaTAShowing = -1;
				$scope.criteriaTAShowing = -1;
				$scope.showTAConcepts = false;
				$scope.showTATargets = false;
				$scope.showTASources = false;
				$scope.showTACriteria = false;
				$scope.showTARemember = false;
			}
			$scope.RefreshTypeAheads();
		};
		
		$scope.RefreshTypeAheads = function() {
			$scope.concepts = null;
			$scope.targets = null;
			$scope.variables = null;
			$scope.concepts = [];
			$scope.targets = [];
			$scope.variables = [];
			for(var i=0;i<$scope.rules.length;i++)
			{
				var rule = $scope.rules[i];
				if ($scope.concepts.indexOf(rule.concept) == -1)
					$scope.concepts.push(rule.concept);
				if ($scope.targets.indexOf(rule.target) == -1)
					$scope.targets.push(rule.target);
				if ($scope.targets.indexOf(rule.response.source) == -1)
					$scope.targets.push(rule.response.source);
				if (rule.criteria != null)
				{
					for(var j=0;j<rule.criteria.length;j++)
					{
						if ($scope.variables.indexOf(rule.criteria[j].name) == -1)
							$scope.variables.push(rule.criteria[j].name);
					}
				}
				if (rule.response.remember != null)
				{
					for(var j=0;j<rule.response.remember.length;j++)
					{
						if ($scope.variables.indexOf(rule.response.remember[j].name) == -1)
							$scope.variables.push(rule.response.remember[j].name);
					}
				}
			}
		};
		/* Initial values */
		$scope.selected = 0;
		$scope.rules = [
			{id: guid(), name:'',concept:'',criteria:$scope.NewCriteriaObj(),target:'',response:{id:guid(),source:'',remember:$scope.NewMemoryObj(),addonStorage:{},then:{target:'',concept:''}}}
		];
		/* Tool Related Functions */
		$scope.ExportFile = function() {
			if ($scope.entryform.$invalid) 
			{
				alert('Please correct the errors outlined in red on the form.');
				return; 
			}
			var text = "";
			var newLine = "\r\n";
			for(var i=0;i<$scope.rules.length;i++)
			{
				var rule = $scope.rules[i];
				var criteria = rule.criteria;
				var response = rule.response;
				var memory = response.remember;
				if (rule.name)
					text += "Rule "+rule.name+newLine;
				else
					text += "Rule "+rule.id+newLine;
				text += "{"+newLine;
				if (rule.concept != null && rule.concept != "")
                {
                    text += "    concept "+rule.concept+newLine;
                }
				if (criteria != null)
				{
					if (criteria.length > 0)
					{
						var critLine = "";
						for(var j=0;j<criteria.length;j++)
						{
							if (criteria[j].name != "" && criteria[j].value != "")
								critLine += " " + criteria[j].name + criteria[j].op + criteria[j].value;
						}
						if (critLine.length > 0)
							text += "    criteria" + critLine + newLine;

					}
				}
				if (rule.target != null && rule.target != "")
				{
					text += "	target "+rule.target+newLine;
				}
				if (rule.response != null)
				{
					text += "	response Response_"+rule.name+newLine;
				}
				text += "}"+newLine+newLine;
				text += "Response Response_"+rule.name+newLine;
				text += "{"+newLine+newLine;
				if (rule.response.source != null && rule.response.source != "")
				{
					text += "	source "+response.source+newLine;
				}
				if (memory.length > 0)
				{
					for(var j=0;j<memory.length;j++)
					{
                        if (memory[j].name != "" && memory[j].value != "")
                            text += "    remember "+memory[j].name+":"+memory[j].value+newLine;
					}
				}
				for(var j=0;j<$scope.addons.length;j++)
				{
					if (response.addonStorage[$scope.addons[j].keyword] != null)
					{
                        for(var k=0;k<response.addonStorage[$scope.addons[j].keyword].length;k++)
                        {
                            text += "    "+$scope.addons[j].keyword+" "+response.addonStorage[$scope.addons[j].keyword][k].value + newLine;
                        }

					}
				}
				if (response.then != null)
				{
					if  (response.then.target)
					{
						text += "	then "+response.then.target+" "+response.then.concept+newLine;
					}
				}
				text += "}"+newLine+newLine;
			}
			var textFileAsBlob = new Blob([text], {type:'text/plain'});
			downloadLink = document.createElement("a");
			downloadLink.download = "main.txt";
			downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
			downloadLink.onclick = destroyClickedElement;
			downloadLink.style.display = "none";
			document.body.appendChild(downloadLink);
			downloadLink.click();
		};
		$scope.ImportFile = function(result) {
			var supportedRuleKeywords = { 
				'concept': function(line) {
					return line.split(/\s/)[1];
				},
				'target': function(line) {
					return line.split(/\s/)[1];
				},
				'criteria': function(line) {
					var criteria = line.split(/\s/);
					var result = new Array();
					if (criteria.length == 0) { result = $scope.NewCriteriaObj; }
					else
					{
						for(var i=1;i<criteria.length;i++)
						{
							var matches = criteria[i].match(/(.+?)(!=|=|<[>=]?|>=?)(.+)/);
							result.push({id:guid(),name:matches[1].trim(),op:matches[2],value:matches[3]});
						}
					}
					return result;
				},
				'response': function(line) {
					return line.split(/\s/)[1];
				}
			}
			var supportedResponseKeywords = { 
				'source': function(line) {
					return line.split(/\s/)[1];
				},
				'remember': function(line) {
					line = line.split(/:/);
					return {id:guid(),name:line[0].trim(),value:line[1]};
				},
				'then': function(line) {
					line = line.split(/\s/);
					return {target: line[1], concept: line[2]};
				}
			}
			var lines = result.split(/\r\n|\r|\n/g);
			// Clear array
			$scope.rules = new Array();
			var processingRule = false;
			var processingResponse = false;
			var currentRule;
			var currentResponse;
			var responses = new Object();
			var blockName;
			for(var i=0;i<lines.length;i++)
			{
				var line = lines[i];
				if (line == "{" || line.Length == 0) { continue; }
				line = line.trim(); 
				var lcLine = line.toLowerCase();
				var keyword = line.split(/\s/)[0].toLowerCase();
				if (!processingRule && !processingResponse)
				{
					if (keyword == "rule")
					{
						processingRule = true;
						currentRule = $scope.rules.push({id:guid()});
						$scope.rules[currentRule-1].name = line.split(/\s/)[1];
						continue;
					}
					if (keyword == "response")
					{
						processingResponse = true;
						currentResponse = line.split(/\s/)[1];
						responses[currentResponse] = {id:guid(),remember:[],addonStorage:{}};
						continue;
					}
				}
				else
				{
					if (keyword == "}")
					{
						if (processingRule) 
						{ 
							processingRule = false; 
						}
						if (processingResponse) 
						{
							processingResponse = false; 
						}
					}
					else
					{
						if (processingRule)
						{
							$scope.rules[currentRule-1][keyword] = supportedRuleKeywords[keyword](line.substring(keyword.length));
						}
						if (processingResponse)
						{
							if (supportedResponseKeywords[keyword] != null)
							{
								if (typeof responses[currentResponse][keyword] === 'object')
									responses[currentResponse][keyword].push(supportedResponseKeywords[keyword](line.substring(keyword.length)));
								else
									responses[currentResponse][keyword] = supportedResponseKeywords[keyword](line.substring(keyword.length));
							}
							else
							{
								for(var j=0;j<$scope.addons.length;j++)
								{
									if ($scope.addons[j].keyword.toLowerCase() == keyword)
									{
                                        if ($scope.addons[j].multiple)
                                        {
                                            if (responses[currentResponse].addonStorage[keyword] == null) { responses[currentResponse].addonStorage[keyword] = new Array(); }
                                            responses[currentResponse].addonStorage[keyword].push({id:guid(),value:line.substring(keyword.length).trim()});
                                        }
                                        else
                                        {
                                            responses[currentResponse].addonStorage[keyword] = line.substring(keyword.length).trim();
                                        }
									}
								}
							}
						}
					}
				}
			}
			//Tie responses to rules
			for(var i=0;i<$scope.rules.length;i++)
			{
				$scope.rules[i].response = responses[$scope.rules[i].response];
			}
			$scope.RefreshTypeAheads();
		}
});
app.filter('startsWith', function() {
	return function(items, val) {
		if (val == null) { return items; }
		else {
			var filtered = [];
			for (var i=0;i<items.length;i++)
			{
				var re = new RegExp('^'+val,'i');
				if (re.test(items[i]) && items[i] !== val)
					filtered.push(items[i]);
			}
			return filtered;
		}
	};
});
app.filter('capitalize', function() {
    return function(input, all) {
      return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
});
app.directive('typeAhead', function() {
	return {
		restrict: 'E',
		scope: {
			
		},
		template: '<ul><li>Test</li></ul>',
		link: function(scope, element, attrs) {
			console.log(element);
		}
	}
});

// If browser doesn't support FileReader...
if (typeof window.FileReader === 'undefined') {
	// Try fallback input button?
	// TODO: How do we get open window and stuff... maybe flash?
	// TODO: Or maybe only support HTML5 browsers.
	importButton.className = '';
}

function destroyClickedElement(event)
{
	document.body.removeChild(event.target);
}

var importContainer = document.getElementById('importContainer');
var sfImportObj = document.getElementById('scriptFileImport');
var addonImportObj = document.getElementById('addonFileImport');
var mainContainer = document.getElementById('mainContainer');
var importButton = document.getElementById('importButton');

function ShowDropZones(e) {
	e.preventDefault();
	e.dataTransfer.setData("text",e.target.id);
	importContainer.className = '';
}

function DragOver(e) {
	e.preventDefault();
	importContainer.className = '';
}

function HideDropZone(e) {
	e = e || window.event; // get window.event if e argument missing (in IE)   
	if (e.preventDefault) { e.preventDefault(); } // stops the browser from redirecting off to the image.
	importContainer.className = 'hide';
	var data = e.dataTransfer.getData("text");

	var dt    = e.dataTransfer;
	var files = dt.files;
	for (var i=0; i<files.length; i++) {
		var file = files[i];
		var reader = new FileReader();
		var controllerElement = document.querySelector('#mainContainer');
		var controllerScope = angular.element(controllerElement).scope();
		reader.readAsText(file);
		reader.onloadend = function(ev) {
			var result = this.result;
			controllerScope.$apply(function() {
				if (file.name.indexOf(".json") > -1)
				{
						controllerScope.addons = JSON.parse(result);
				} else {				
						controllerScope.ImportFile(result);
				}
			});
		}
	}
	return false;
}


/**
 * Generates a GUID string.
 * @returns {String} The generated GUID.
 * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
 * @author Slavik Meltser (slavik@meltser.info).
 * @link http://slavik.meltser.info/?p=142
 */
function guid() {
    function _p8(s) {
        var p = (Math.random().toString(16)+"000000000").substr(2,8);
        return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

// Warn about back nav
document.onkeydown = function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' && 
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' || 
                 d.type.toUpperCase() === 'FILE' || 
                 d.type.toUpperCase() === 'EMAIL' || 
                 d.type.toUpperCase() === 'SEARCH' || 
                 d.type.toUpperCase() === 'DATE' )
             ) || 
             d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = !confirm("Backspace triggers the browser's Back function.\nThat'd be a bummer if you haven't exported in awhile.\nIf you really want to do that, click OK.");;
        }
    }

    if (doPrevent) {
        event.preventDefault();
    }
};

 function GetOffset (object, offset) {
	if (!object)
		return;
	offset.x += object.offsetLeft;
	offset.y += object.offsetTop;

	GetOffset (object.offsetParent, offset);
}

function GetScrolled (object, scrolled) {
	if (!object)
		return;
	scrolled.x += object.scrollLeft;
	scrolled.y += object.scrollTop;

	if (object.tagName.toLowerCase () != "html") {
		GetScrolled (object.parentNode, scrolled);
	}
}