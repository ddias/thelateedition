﻿var addonData = [
	{
		'keyword': 'voice',
		'helper': 'Sound file ID'
		,'multiple': 'true'
	},
	{
		'keyword': 'commentary',
		'helper': 'Sound file ID'
		,'multiple': 'true'
	},
	{
		'keyword': 'sfx',
		'helper': 'Sound file ID'
		,'multiple': 'true'
	},
	{
		'keyword': 'imageswap',
		'helper': 'Image name'
		,'multiple': 'true'
	},
	{
		'keyword': 'scenechange',
		'helper': 'Scene name'
		,'multiple': 'true'
	}
];
