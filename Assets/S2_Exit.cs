﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using EventMaster;

public class S2_Exit : PickupItem
{
    public bool exitable = false;
    public PickupItem Gun;
    public PickupItem Journal;
    public GameObject FinishScreen;

    void Start()
    {
        // For some reason not putting this functionality in Start makes invisible itesm
        controller = GameObject.Find("Controller");
        ep = GetComponent<EventParticipant>();
        ExecuteEvents.Execute<IInventoryHandler>(controller, null, (x, y) => x.AddItem(Gun.gameObject));
        ExecuteEvents.Execute<IInventoryHandler>(controller, null, (x, y) => x.AddItem(Journal.gameObject));
    }

    //// Update is called once per frame
    //void Update () {

    //}

    //public override void Take()
    //{
    //    //if (exitable)
    //    //{
    //    //    FinishScreen.SetActive(true);
    //    //    Debug.Log("InteractableItem::Take()");
    //    //    if (ep != null)
    //    //        ep.PerformQuery("take");
    //    //}
    //}

    

    public override void OnPointerClick(PointerEventData eventData)
    {
        if (exitable && (!Game.Instance.CommentaryMode || eventData.used))
        {
            if (Game.Instance.State == GameState.Intro)
                return;
            // Move to this object and get it
            ExecuteEvents.Execute<IWalkToTarget>(controller, eventData, (x, y) => x.WalkTo(eventData.position));
            //ExecuteEvents.Execute<IInteractHandler>(controller, eventData, (x, y) => x.Interact(InteractionType.Take, ExecuteInteraction));
            ep.PerformQuery("take");
            FinishScreen.SetActive(true);
        }
    }
}
