﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using EventMaster;

public class OpeningInteraction : InteractableItem
{
    public GameObject[] UIElements;
    SpriteRenderer render;
    bool fading;
    float speed = 0.01f;
    float timer;

    public override void OnPointerClick(PointerEventData eventData)
    {
        if (!fading)
        {
            if (controller == null)
                controller = GameObject.Find("Controller");
            if (ep == null)
                ep = GetComponent<EventParticipant>();
            ep.PerformQuery("take");
            render = GetComponent<SpriteRenderer>();
            fading = true;
        }
    }

    void Update()
    {
        if (fading)
        {
            Color c = render.color;

            c.a = Mathf.Lerp(1.0F, 0.0F, timer*speed);
            timer += Time.deltaTime;
            if (Game.Instance.DebugMode)
            {
                fading = false;
                c.a = 0;
            }
            render.color = c;
            if (c.a == 0)
            {
                foreach(GameObject el in UIElements)
                {
                    // Send message is just easier than creating a new event
                    el.SendMessage("EnableMe");
                }
                Destroy(transform.parent.gameObject);
            }
        }
    }

    public void DoFadeIn()
    {
        if (controller == null)
            controller = GameObject.Find("Controller");
        if (ep == null)
            ep = GetComponent<EventParticipant>();
        ep.PerformQuery("take");
        render = GetComponent<SpriteRenderer>();
        fading = true;
    }
}
