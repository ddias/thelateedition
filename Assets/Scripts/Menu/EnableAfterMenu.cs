﻿using UnityEngine;
using System.Collections;

public class EnableAfterMenu : MonoBehaviour
{
    public Vector2 hiddenPos;
    Vector3 showingPos;

    public void Awake()
    {
        // Originally placed in position, so grab that
        showingPos = GetComponent<RectTransform>().anchoredPosition;
        // Move to hidden position
        GetComponent<RectTransform>().anchoredPosition = hiddenPos;
    }

    public void EnableMe()
    {
        GetComponent<RectTransform>().anchoredPosition = showingPos;
    }
}
