﻿using UnityEngine;
using System.Collections;

public class StartingMenu : MonoBehaviour
{

    public GameObject[] UIElementsToHide;
    public GameObject[] RelatedMenuElements;

	// Use this for initialization
	void Start ()
    {
	    foreach(GameObject obj in UIElementsToHide)
        {
            obj.SetActive(false);
        }
        foreach (GameObject obj in RelatedMenuElements)
        {
            obj.SetActive(true);
        }
    }

    public void ClickToStart()
    {
        foreach (GameObject obj in UIElementsToHide)
        {
            obj.SetActive(true);
            if (Game.Instance.DebugMode)
            {
                obj.SendMessage("EnableMe");
            }
        }
        foreach (GameObject obj in RelatedMenuElements)
        {
            obj.SetActive(false);
        }
    }
}
