﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using EventMaster;

public class LockedContainer : Container, ILockControlHandler
{
    public bool Locked = true;
    public GameObject UnlockUI;

    public void Lock()
    {
        GetComponent<EventParticipant>().PerformQuery("locked");
        Locked = true;
    }

    public void Unlock()
    {
        GetComponent<EventParticipant>().PerformQuery("unlocked");
        Locked = false;
    }

    public override void TryCombine(GameObject obj)
    {
        foreach (string key in Keys)
        {
            if (obj.name.ToLower() == key.ToLower())
            {
                Unlock();
                ExecuteEvents.Execute<ICombineHandler>(obj, null, (x, y) => x.CombineSuccess(obj));
                break;
            }
        }
        if (Locked)
        {
            ExecuteEvents.Execute<ICombineHandler>(obj, null, (x, y) => x.CombineFail(obj));
            GetComponent<EventParticipant>().PerformQuery("locked");
        }
        else
            base.TryCombine(obj);
    }
}

