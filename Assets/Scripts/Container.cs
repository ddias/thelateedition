﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using EventMaster;

public class Container : DropOffItem
{
    public List<InteractableItem> InitialInventory;

    protected Queue<InteractableItem> inventory;

    public override void Init()
    {
        inventory = new Queue<InteractableItem>();
        foreach (InteractableItem itm in InitialInventory)
            inventory.Enqueue(itm);
        base.Init();
    }

    public override void TryCombine(GameObject obj)
    {
        // check if item is in key
        foreach (string key in Keys)
        {
            // check if the object dropped in is in the Keys list.
            if (obj.name.ToLower() == key.ToLower())
            {
                Debug.Log("item allowed in container, stowing");
                stow_in_container(obj);
                return;
            }
        }
        Debug.Log("item not allowed in container, returning");
        CombineFail(obj);

    }

    // helper for Container, which puts the combine object in the container
    public void stow_in_container(GameObject obj)
    {
        Debug.Log("Container::CombineSuccess()");
        string name = obj.name;
        Memory.Instance.memory["using_" + obj.name.ToLower()] = 1;
        Debug.Log("using_" + obj.name.ToLower() + " is set to 1");
        GetComponent<EventParticipant>().PerformQuery("combine");
        Destroy(obj);
        GameObject o = GameObject.Find(name);
        inventory.Enqueue(o.GetComponent<InteractableItem>());
        o.transform.position = new Vector3(-99, -99, 0);
    }

    public override void Take()
    {
        Debug.Log("Container::Take()");
        // Can't take containers, so don't call base... but you can take from them!
        if (inventory.Count > 0)
        {
            GetComponent<EventParticipant>().PerformQuery("take");
            // TODO: Maybe implement a inventory screen for containers... but until then it's a queue
            InteractableItem itm = inventory.Dequeue();
            itm.Take();
        }
        else
            GetComponent<EventParticipant>().PerformQuery("empty");
    }
}
