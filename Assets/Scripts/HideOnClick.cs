﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class HideOnClick : MonoBehaviour, IPointerClickHandler
{ 
    // Use this for initialization
    void Start () 
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        gameObject.SetActive(false);
    }
}
