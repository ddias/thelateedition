﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

[RequireComponent(typeof(Collider2D))]
public class WalkableArea : MonoBehaviour, IPointerClickHandler
{

    private GameObject controller;

    void Start()
    {
        controller = GameObject.Find("Controller");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ExecuteEvents.Execute<IWalkToTarget>(controller, eventData, (x, y) => x.WalkTo(eventData.position));
    }
}

public interface IWalkToTarget : IEventSystemHandler
{
    void WalkTo(Vector3 pos);
}
