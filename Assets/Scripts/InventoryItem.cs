﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

/// <summary>
/// Added to an item when it goes into your inventory
/// </summary>
public class InventoryItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, ICombineHandler
{
    private RectTransform t;
    private Vector2 originalPos;

    void Start()
    {
        GetComponent<UnityEngine.UI.Image>().preserveAspect = true;
        t = GetComponent<RectTransform>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        originalPos = t.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        t.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Collider2D hitCollider = Physics2D.OverlapPoint(Camera.main.ScreenToWorldPoint(eventData.position));
        if (hitCollider && hitCollider.GetComponent<InteractableItem>())
        {
            ExecuteEvents.Execute<ICombineHandler>(hitCollider.gameObject, eventData, (x, y) => x.TryCombine(gameObject));
        }
        else
        {
            t.position = originalPos;
        }
    }

    public void TryCombine(GameObject obj)
    {
        // TODO: Figure out how combining items within the inventory works
    }

    public void CombineSuccess(GameObject item)
    {
        Debug.Log("InventoryItem::CombineSuccess");
        Destroy(gameObject);
    }

    public void CombineFail(GameObject item)
    {
        Debug.Log("InventoryItem::CombineFail");
        t.position = originalPos;
    }
}
