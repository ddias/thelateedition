﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using EventMaster;

public class CommentaryModeController : MonoBehaviour
{
    public GameObject IntroCommentary;

    //public bool commentarys_enabled_from_start;
    //void Awake()
    //{
    //    if (commentarys_enabled_from_start)
    //    {
    //        Game.Instance.PushState(GameState.Intro);
    //    }
    //}

    void Awake()
    {

    }

    void Start () {
	
	}

    // Update is called once per frame
    void Update() {
        //Debug.Log(Memory.Instance.memory["CommentaryMode"]);
    }

    public void TurnOnCommentary()
    {
        Game.Instance.CommentaryMode = true;
        Memory.Instance.memory["CommentaryMode"] = 1;
        if (Game.Instance.State == GameState.Intro && IntroCommentary != null)
        {
            IntroCommentary.SetActive(true);
        }
        CommentaryItem[] cObjs = FindObjectsOfType<CommentaryItem>();
        foreach(CommentaryItem itm in cObjs)
        {
            ExecuteEvents.Execute<ICommentaryTarget>(itm.gameObject, null, (x, y) => x.CommentaryOn());
        }
    }

    public void TurnOffCommentary()
    {
        Game.Instance.CommentaryMode = false;
        Memory.Instance.memory["CommentaryMode"] = 0;

        CommentaryItem[] cObjs = FindObjectsOfType<CommentaryItem>();
        foreach (CommentaryItem itm in cObjs)
        {
            ExecuteEvents.Execute<ICommentaryTarget>(itm.gameObject, null, (x, y) => x.CommentaryOff());
        }
    }
}
