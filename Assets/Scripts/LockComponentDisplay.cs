﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LockComponentDisplay : MonoBehaviour
{
    public void UpdateDisplay(char[] guess)
    {
        if (guess != null)
        {
            for(int i=0;i<guess.Length;i++)
            {
                Text t = transform.GetChild(i).GetComponent<Text>();
                if (t != null)
                {
                    t.text = guess[i].ToString();
                }
            }
        }
    }
}
