﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LockComponentButtonArray : MonoBehaviour
{
	void Start()
    {
        Button[] btns = GetComponentsInChildren<Button>();
        foreach(Button btn in btns)
        {
            string val = btn.GetComponentInChildren<Text>().text;
            btn.onClick.AddListener(() =>
            {
                SendMessageUpwards("SetValue", val);
            });
        }
    }
}
