﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;
using EventMaster;

public class SceneManager : MonoBehaviour, ISceneSwitcherTarget
{
    public void LoadScene(string sceneName)
    {
        Application.LoadLevel(sceneName);
    }
}

public interface ISceneSwitcherTarget : IEventSystemHandler
{
    void LoadScene(string sceneName);
}