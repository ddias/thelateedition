﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using EventMaster;

public class DropOffItem : InteractableItem, ICombineHandler
{
    public List<string> Keys;

    public virtual void TryCombine(GameObject obj)
    {
        Debug.Log("DropOffItem::TryCombine()");
        MonoBehaviour script = obj.GetComponent<InteractableItem>();
        if (script == null)
            script = obj.GetComponent<InventoryItem>();
        if (script != null)
        {
            // Check if this item can be combined with
            foreach (string key in Keys)
            {
                if (obj.name.ToLower() == key.ToLower())
                {
                    CombineSuccess(obj);
                    return;
                }
            }
            CombineFail(obj);
        }
    }

    public virtual void CombineSuccess(GameObject item)
    {
        Debug.Log("DropOffItem::CombineSuccess()");
        Memory.Instance.memory["using_"+item.name.ToLower()] = 1;
        ep.PerformQuery("combine");
        ExecuteEvents.Execute<ICombineHandler>(item, null, (x, y) => x.CombineSuccess(gameObject));
    }

    public virtual void CombineFail(GameObject item)
    {
        Debug.Log("DropOffItem::CombineFail()");
        Memory.Instance.memory["using_" + item.name.ToLower()] = 1;
        ep.PerformQuery("combine");
        ExecuteEvents.Execute<ICombineHandler>(item, null, (x, y) => x.CombineFail(gameObject));
    }
}
