﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System;

public class Inventory : MonoBehaviour, IInventoryHandler
{
    public SortedList Items = new SortedList();
    public GameObject InventoryPanel;
    public GameObject InventoryItems;

    public GameObject ItemPrefab;

    private bool invShowing;
    private bool sliding;
    Vector2 slideTarget;
    RectTransform trans;
    Vector2 lerpStart;
    readonly float lerpTime = 3;
    float lerpCurrentTime;

    void Start()
    {
        invShowing = false;
        sliding = false;
        trans = InventoryPanel.GetComponent<RectTransform>();
    }

    void Update()
    {
        if (sliding)
        {
            lerpCurrentTime += Time.deltaTime;
            if (lerpCurrentTime >= lerpTime)
            {
                lerpCurrentTime = lerpTime;
                sliding = false;
            }
            float t = Mathf.Sin(lerpCurrentTime / lerpTime * Mathf.PI * 0.5f); // Ease in
            trans.anchoredPosition = Vector2.Lerp(trans.anchoredPosition, slideTarget, t);
        }
    }

    public void Toggle()
    {
        if (Game.Instance.State == GameState.Intro) { return; }
        // Toggle if inventory is showing and setup lerp
        invShowing = !invShowing;
        sliding = true;
        lerpCurrentTime = 0;
        lerpStart = trans.anchoredPosition;
        if  (invShowing)
            slideTarget = new Vector2(0, trans.anchoredPosition.y);
        else
            slideTarget = new Vector2(-120, trans.anchoredPosition.y);
    }

    public void QuickShow()
    {
        StartCoroutine("TimedToggle");
    }

    public IEnumerator TimedToggle()
    {
        Toggle();
        yield return new WaitForSeconds(0.35F);
        Toggle();
    }

    public void AddItem(GameObject obj)
    {
        // TODO: What to do with the in world item? Send it to oblivion for now.
        obj.transform.position = new Vector3(-99, -99);
        SpriteRenderer spr = obj.GetComponent<SpriteRenderer>();
        GameObject item = Instantiate(ItemPrefab);
        item.name = obj.name;
        item.transform.SetParent(InventoryItems.transform);
        item.GetComponent<Image>().sprite = spr.sprite;
        Items.Add(Items.Count, item);
    }

    public void RemoveItem(GameObject obj)
    {
        int idx = -1;
        for(int i=0;i<Items.Count;i++)
        {
            if (obj == Items[i])
            {
                idx = i;
            }
        }
        if (idx != -1)
            Items.RemoveAt(idx);
        // Do something useful with the item
        obj.transform.SetParent(null);
        Destroy(obj.GetComponent<InventoryItem>());
    }
}

public interface IInventoryHandler : IEventSystemHandler
{
    void Toggle();
    void QuickShow();
    void AddItem(GameObject obj);
    void RemoveItem(GameObject obj);
}