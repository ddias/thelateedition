﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class CombinationPuzzle : MonoBehaviour, IComboPuzzleHandler
{
    [Tooltip("ONLY ENTER NUMBERS. IS A STRING BECAUSE YOU CAN'T HAVE 0 AT THE BEGINNING IF NOT")]
    public string Key;
    [Tooltip("Leave empty for the GameObject this component is on.")]
    public GameObject LockFor;

    protected char[] guess;
    protected int index=0;

    // Use this for initialization
    void Start()
    {
        RandomizeGuess();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void MakeGuess()
    {
        if (new string(guess) == Key)
        {
            ExecuteEvents.Execute<ILockControlHandler>(LockFor == null ? gameObject : LockFor, null, (x, y) => x.Unlock());
            transform.parent.gameObject.SetActive(false);
        }
    }

    public void RandomizeGuess()
    {
        int idxs = Key.Length;
        // Initialize our guess
        guess = new char[idxs];
        for(int i=0;i<idxs;i++)
        {
            int target = Convert.ToInt32(Key.ToCharArray()[i]);
            // Assuming numbers
            int d = UnityEngine.Random.Range(0, 9);
            // Avoid the number that the key actually is
            while (d == target)
            {
                d = UnityEngine.Random.Range(0, 9);
            }
            SetValue(d.ToString());
        }
    }

    public void SetValue(string value)
    {
        // See if value is a valid character, otherwise submit guess
        try
        {
            int v = Convert.ToInt32(value);
            guess[index] = value.ToCharArray()[0];
            index++;
            if (index >= Key.Length)
                index = 0;
            GetComponentInChildren<LockComponentDisplay>().UpdateDisplay(guess);
        }
        catch(FormatException ex)
        {
            MakeGuess();
        }
    }

    public void SetValue(int idx, char value)
    {
        guess[idx] = value;
    }
}

public interface IComboPuzzleHandler : IEventSystemHandler
{
    void SetValue(string value);
    void MakeGuess();
    void RandomizeGuess();
}

public interface ILockControlHandler : IEventSystemHandler
{
    void Lock();
    void Unlock();
}