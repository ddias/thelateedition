﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class SoundManager : MonoBehaviour, ISoundPlayerTarget
{
    public bool DEBUG_MODE;
    public List<AudioClip> clips;
    public AudioSource Commentary;
    public AudioSource MainCharacter;
    public AudioMixerGroup fxGroup;

    private AudioSourcePool fxPool;

    // Priority for commentary
    private int currentPriority;

    // Use this for initialization
    void Start()
    {
        transform.position = Camera.main.transform.position;
        fxPool = new AudioSourcePool(gameObject, 2);
        if (DEBUG_MODE)
            Game.Instance.DebugMode = true;
    }

    public void Play(string clipid)
    {
        Play(clipid, 1);
    }
    public void Play(string clipid, int priority)
    {
        if (Commentary.isPlaying)
        {
            if (priority > currentPriority)
            {
                Debug.Log(String.Format("Audio '{0}' Main Character track swallowed by higher priority sound playing. {1} > {2}. Priority is reverse order.", clipid, priority, currentPriority));
                return;
            }
            MainCharacter.Stop();
        }
        currentPriority = priority;
        var clip = (from c in clips where c.name.ToLower() == clipid.Replace("\"","").ToLower() select c);
        if (clip.Any())
        {
            MainCharacter.clip = clip.First();
            MainCharacter.Play();
        }
        else
        {
            Debug.LogWarningFormat("Requested to play character voice {0} but it has not been attached to the SoundManager.", clipid);
        }
        if (Game.Instance.DebugMode) { StartCoroutine("DEBUGSTOP"); }
    }

    public void PlaySFX(string clipid)
    {
        AudioSource src = fxPool.Fetch();
        var clip = (from c in clips where c.name.ToLower() == clipid.Replace("\"", "").ToLower() select c);
        if (clip.Any())
        {
            src.clip = clip.First();
            src.Play();
        }
        else
        {
            Debug.LogWarningFormat("Requested to play sound {0} but it has not been attached to the SoundManager.", clipid);
        }
    }

    public void PlayCommentary(string clipid)
    {
        PlayCommentary(clipid, 1);
    }

    public void PlayCommentary(string clipid, int priority)
    {
        if (Commentary.isPlaying)
        {
            if (priority > currentPriority)
            {
                Debug.Log(String.Format("Audio '{0}' on Commentary track swallowed by higher priority sound playing. {1} > {2}. Priority is reverse order.", clipid, priority, currentPriority));
                return;
            }
            MainCharacter.Stop();
        }
        currentPriority = priority;
        var clip = (from c in clips where c.name.ToLower() == clipid.Replace("\"", "").ToLower() select c);
        if (clip.Any())
        {
            MainCharacter.clip = clip.First();
            MainCharacter.Play();
            if (Game.Instance.DebugMode) { StartCoroutine("DEBUGSTOP"); }
        }
        else
        {
            Debug.LogWarningFormat("Requested to play commentary {0} but it has not been attached to the SoundManager.", clipid);
        }
    }

    IEnumerator DEBUGSTOP()
    {
        yield return new WaitForSeconds(2);
        MainCharacter.Stop();
        Commentary.Stop();
    }
}

public interface ISoundPlayerTarget : IEventSystemHandler
{
    void Play(string clipid);
    void Play(string clipid, int priority);
    void PlayCommentary(string clipid);
    void PlayCommentary(string clipid, int priority);
    void PlaySFX(string clipid);
}

public class AudioSourcePool : Queue
{
    private GameObject gameObject;

    public AudioSourcePool(GameObject obj)
    {
        gameObject = obj;
        Store(gameObject.AddComponent<AudioSource>());
    }
    public AudioSourcePool(GameObject obj, int initialSize)
    {
        gameObject = obj;
        for (int i = 0; i < initialSize; i++)
        {
            Store(gameObject.AddComponent<AudioSource>());
        }
    }

    public AudioSource Fetch()
    {
        AudioSource src = null;
        for (int i = 0; i < Count; i++)
        {
            src = (AudioSource)Dequeue();
            if (src.isPlaying)
            {
                Store(src);
                src = null;
            }
            else
            {
                break;
            }
        }
        if (src == null)
        {
            src = gameObject.AddComponent<AudioSource>();
            Store(src);
        }
        return src;
    }

    public void Store(AudioSource src)
    {
        Enqueue(src);
    }
}