﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class PickupItem : InteractableItem {
    public override void Take()
    {
        Debug.Log("Pickup Item");
        ExecuteEvents.Execute<IInventoryHandler>(controller, new BaseEventData(EventSystem.current), (x, y) => x.AddItem(gameObject));
        ExecuteEvents.Execute<IInventoryHandler>(controller, null, (x, y) => x.QuickShow());
        base.Take();
    }
}
