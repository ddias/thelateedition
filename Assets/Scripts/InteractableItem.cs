﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;
using EventMaster;

[RequireComponent(typeof(Collider2D))]
public class InteractableItem : MonoBehaviour, IPointerClickHandler
{
    protected GameObject controller;
    protected EventParticipant ep;

    void Awake()
    {
        Init();
    }

    public virtual void Init()
    {
        controller = GameObject.Find("Controller");
        ep = GetComponent<EventParticipant>();
    }

    public virtual void OnPointerClick(PointerEventData eventData)
    {
        if (!Game.Instance.CommentaryMode || eventData.used)
        {
            if (Game.Instance.State == GameState.Intro)
                return;
            // Move to this object and get it
            ExecuteEvents.Execute<IWalkToTarget>(controller, eventData, (x, y) => x.WalkTo(eventData.position));
            ExecuteEvents.Execute<IInteractHandler>(controller, eventData, (x, y) => x.Interact(InteractionType.Take, ExecuteInteraction));
        }
    }

    public virtual void ExecuteInteraction(InteractionType type)
    {
        Debug.Log(type);
        switch (type)
        {
            case InteractionType.Look:
                Look();
                break;
            case InteractionType.Talk:
                Talk();
                break;
            case InteractionType.Take:
                Take();
                break;
            default:
                break;
        }
    }

    public virtual void Look()
    {
        if (ep != null)
            ep.PerformQuery("look");
    }

    public virtual void Talk()
    {
        if (ep != null)
            ep.PerformQuery("talk");
    }

    public virtual void Take()
    {
        Debug.Log("InteractableItem::Take()");
        if (ep != null)
            ep.PerformQuery("take");
    }
}

public enum InteractionType { Look, Talk, Take }

public interface IInteractHandler : IEventSystemHandler
{
    void Interact(InteractionType type, InteractionCommand callback);
    void Interact(InteractionType type, InteractionCommand callback, bool waitToBeClose);
}

public interface ICombineHandler : IEventSystemHandler
{
    void TryCombine(GameObject obj);
    void CombineSuccess(GameObject item);
    void CombineFail(GameObject item);
}

public delegate void InteractionCommand(InteractionType type);