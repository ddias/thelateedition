﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class ClickToMove : MonoBehaviour, IWalkToTarget, IInteractHandler {

    public GameObject avatar;
    public float moveSpeed = 3;
    public float turnSpeed = 5;

    public float InteractDistance = 2F;

    public UnityEvent OnArrive;

    private bool moving;
    private bool turning;
    private Vector3 targetPosition;
    private Quaternion originalRot;
    private float timer;

    private InteractionType interactType;
    private InteractionCommand command;

    void Update()
    {
        if (turning)
        {
            var dir = targetPosition - avatar.transform.position;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 270;
            avatar.transform.rotation = Quaternion.Lerp(originalRot,Quaternion.AngleAxis(angle, Vector3.forward),timer);
            timer += Time.deltaTime * turnSpeed;
            if (timer >= 1)
            {
                turning = false;
                moving = true;
            }
        }
        if (moving)
        {
            // Check to see if we're close to the target or not
            Vector3 diff = targetPosition - avatar.transform.position;
            if (diff.sqrMagnitude > InteractDistance*InteractDistance)
            {
                avatar.transform.position = Vector3.MoveTowards(avatar.transform.position, targetPosition, Time.deltaTime * moveSpeed);
            }
            else
            {
                moving = false;
                OnArrive.Invoke();
                if (command != null)
                {
                    command(interactType);
                }
            }
        }
    }

    public void WalkTo(Vector3 pos)
    {
        command = null;
        pos.z = transform.position.z - Camera.main.transform.position.z;
        targetPosition = Camera.main.ScreenToWorldPoint(pos);
        targetPosition.z = 0;
        originalRot = avatar.transform.rotation;
        timer = 0;

        moving = false;
        turning = true;
    }

    public void Interact(InteractionType type, InteractionCommand cmd)
    {
        Interact(type, cmd, true);
    }

    public void Interact(InteractionType type, InteractionCommand cmd, bool waitToBeClose)
    {
        if (waitToBeClose)
        {
            // Store this interaction
            interactType = type;
            command = cmd;
        }
        else
        {
            cmd(type);
        }
    }
}
