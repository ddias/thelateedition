﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Collections;
using EventMaster;
using System.Linq;
using System;

public class ImageManager : MonoBehaviour, ISpriteSwapHandler
{
    public List<Sprite> Sprites = new List<Sprite>();

    public void SwapImage(string objectName, string imageName)
    {
        EventParticipant[] particpants = FindObjectsOfType<EventParticipant>();
        var obj = (from p in particpants where p.id.ToLower() == objectName.ToLower() select p.gameObject);
        if (obj.Any())
        {
            var newSprite = (from s in Sprites where s.name.ToLower() == imageName.ToLower() select s);
            if (newSprite.Any())
            {
                SpriteRenderer spr = obj.First().GetComponent<SpriteRenderer>();
                spr.sprite = newSprite.First();
            }
            else
            {
                Debug.LogWarning("Did not find " + imageName + " in sprite list on " + gameObject.name);
            }
        }
        else
        {
            Debug.LogWarning("Did not find " + objectName + ".");
        }
    }
}

public interface ISpriteSwapHandler : IEventSystemHandler
{
    void SwapImage(string objectName, string imageName);
}