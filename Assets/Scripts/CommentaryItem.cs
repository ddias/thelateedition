﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using EventMaster;

[RequireComponent(typeof(Collider2D))]
public class CommentaryItem : MonoBehaviour, IPointerClickHandler, ICommentaryTarget
{
    [HeaderAttribute("Behavior Flags")]
    public bool BlockGameInteraction = true;
    public bool DestroyWhenDone;

    [HeaderAttribute("Visualization")]
    public bool ShowIcon = true;
    public Sprite iconSprite;

    protected GameObject icon;
    protected GameObject controller;
    protected EventParticipant ep;

    void Awake()
    {
        if(Application.isPlaying)
            Init();
    }

    public virtual void Init()
    {
        icon = new GameObject("icon");
        SpriteRenderer sprRend = icon.AddComponent<SpriteRenderer>();
        sprRend.sprite = iconSprite;
        icon.transform.SetParent(transform);
        Vector2 offset = new Vector2(0, 0);
        if (GetComponent<Collider2D>() != null)
            offset = GetComponent<Collider2D>().offset;
        icon.transform.localPosition = offset;
        sprRend.sortingLayerName = "Commentary";
        icon.SetActive(false);
        controller = GameObject.Find("Controller");
        ep = GetComponent<EventParticipant>();
    }

    public virtual void OnPointerClick(PointerEventData eventData)
    {
        // Don't re-call this event
        if (eventData.used) { return; }
        if (Game.Instance.CommentaryMode)
        {
            DoCommentary();
            if (!BlockGameInteraction)
            {
                eventData.Use();
                ExecuteEvents.Execute<IPointerClickHandler>(gameObject, eventData, (x, y) => x.OnPointerClick(eventData));
            }
            if (DestroyWhenDone)
            {
                Destroy(gameObject);
            }
        }
    }

    public virtual void DoCommentary()
    {
        if (icon.activeSelf)
            ep.PerformQuery("commentary");
    }

    public virtual void CommentaryOn()
    {
        if (Game.Instance.State == GameState.Intro)
            return;
        icon.SetActive(true);
    }

    public virtual void CommentaryOff()
    {
        icon.SetActive(false);
    }
}

public interface ICommentaryTarget : IEventSystemHandler
{
    void CommentaryOn();
    void CommentaryOff();
}