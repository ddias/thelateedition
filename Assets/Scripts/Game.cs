﻿using System;
using System.Collections.Generic;

// Add any additional states you will use
public enum GameState { Started, Intro, Paused }

public sealed class Game
{
    private static volatile Game instance;
    private static object syncRoot = new Object();

    private Game()
    {
        PushState(GameState.Started);
    }

    public static Game Instance
    {
        get
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new Game();
                }
            }

            return instance;
        }
    }

    private Stack<GameState> stateStack = new Stack<GameState>();
    private Dictionary<string, object> gameVars = new Dictionary<string, object>();
    
    public bool CommentaryMode { get; set; }
    public bool DebugMode { get; set; }
    public GameState State { get { return stateStack.Peek(); } }

    public void PushState(GameState state)
    {
        stateStack.Push(state);
    }

    public GameState PopState()
    {
        // If we Pop out the last state, the user wants to close the game?
        if (stateStack.Peek() == GameState.Started)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
         UnityEngine.Application.Quit();
#endif
        }
        return stateStack.Pop();
    }

    public void SetState(GameState state)
    {
        stateStack.Clear();
        stateStack.Push(GameState.Started);
        stateStack.Push(state);
    }
}