﻿using UnityEngine;
using System.Collections.Generic;
using EventMaster;

public class Dispenser : Container
{
    public override void TryCombine(GameObject obj)
    {
        Debug.Log("Dispenser::TryCombine()");
        foreach (string key in Keys)
        {
            if (key.ToLower() == obj.name.ToLower())
            {
                // TODO: This will probably cause issues in the future, but there may be no future! Live in the now!
                base.Take();
                base.TryCombine(obj);
                return;
            }
        }
        base.CombineFail(obj);
    }

    public override void Take()
    {
        Debug.Log("Dispenser::Take()");
        if (Keys.Count == 0)
        {
            base.Take();
        }
    }
}