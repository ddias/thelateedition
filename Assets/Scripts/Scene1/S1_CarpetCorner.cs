﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using EventMaster;

public class S1_CarpetCorner : DropOffItem
{
    [HeaderAttribute("Behavior Flags")]
    [SerializeField]
    private bool hasCrowbar;
    [SerializeField]
    private bool hasBeenAppled;
    [SerializeField]
    private bool hasBeenKeyed;
    [SerializeField]
    private string CrowbarID = "crowbar";
    [SerializeField]
    private string KeyID = "floorkey";
    [SerializeField]
    private string AppleID = "apple";
    [SerializeField]
    public bool amlocked = true;

    public override void TryCombine(GameObject obj)
    {
        Debug.Log("S1_CarpetCorner::TryCombine()");
        MonoBehaviour script = obj.GetComponent<InteractableItem>();
        if (script == null)
            script = obj.GetComponent<InventoryItem>();
        if (script != null)
        {
            // Check if this item can be combined with
            // crowbar first
            if (!hasCrowbar && !hasBeenAppled)
            {
                Debug.LogFormat("combining {0} and {1}",obj.name.ToLower(), CrowbarID.ToLower());
                if (obj.name.ToLower() == CrowbarID.ToLower())
                {
                    hasCrowbar = true;
                    CombineSucessAndStoCrowbar(obj);
                    CombineSuccess(obj);
                    Memory.Instance.memory[""] = 0;
                    return;
                }

            }
            // key next
            else if (!hasBeenKeyed && !hasBeenAppled) // has Crowbar, needs key
            {
                Debug.LogFormat("combining {0} and {1}", obj.name, KeyID.ToLower());
                if (obj.name.ToLower() == KeyID.ToLower())
                {
                    hasBeenKeyed = true;
                    CombineSuccess(obj);
                    return;
                }
            }
            // apple next
            else if (!hasBeenAppled)// has Crowbar, has key, needs apple
            {
                Debug.LogFormat("combining {0} and {1}", obj.name, AppleID.ToLower());
                if (obj.name.ToLower() == AppleID.ToLower())
                {
                    hasBeenAppled = true;
                    CombineSuccess(obj);
                    amlocked = false;
                    // Hook into EVM
                    GetComponent<EventParticipant>().PerformQuery("open");
                    return;
                    // image swap open trapdoor
                }
            }
            CombineFail(obj);
        }
    }

    public override void Take()
    {
        Debug.Log("S1_CarpetCorner::Take");
        //floor dispenses if open
        if (hasCrowbar && hasBeenKeyed && hasBeenAppled && (StoredTreasure != null))
        {
            Debug.Log("trapdoor open");
            if (StoredTreasure != null)
            {
                InteractableItem ItemInteraction = StoredTreasure.GetComponent<InteractableItem>();
                if (ItemInteraction != null)
                {
                    //GetComponent<EventParticipant>().PerformQuery("take");
                    ItemInteraction.Take();
                    StoredTreasure = null;
                }
                else { Debug.Log("Tried to take from non Interactable"); }
            }
            else
            {
                Debug.Log("no item in carpet corner");
            }
        }
        //floor give back crowbar if it has one
        else if (hasCrowbar)
        {
            Debug.Log("getting crowbar back");
            if (StoredCrowbar)
            {
                
                InteractableItem ItemInteraction = StoredCrowbar.GetComponent<InteractableItem>();
                if (ItemInteraction != null)
                {
                    //GetComponent<EventParticipant>().PerformQuery("take");
                    ItemInteraction.Take();
                    hasCrowbar = false;
                    StoredCrowbar = null;
                }
                else { Debug.Log("Tried to take from non Interactable"); }

            }
            

        }
        //otherwise do nothing
       
    }

    public GameObject StoredCrowbar;
    public GameObject StoredTreasure;

    public void CombineSucessAndStoCrowbar(GameObject item)
    {
        string name = item.name;
        Destroy(item);
        GameObject o = GameObject.Find(name); // this gets the interactable instead of the inventory item
        StoredCrowbar = o;
        o.transform.position = new Vector3(-99, -99, 0);

    }
}


