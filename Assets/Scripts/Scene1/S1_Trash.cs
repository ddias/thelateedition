﻿using UnityEngine;
using System.Collections;
using EventMaster;

public class S1_Trash : Dispenser
{
    public override void Take()
    {
        if (Memory.Instance.memory.ContainsKey("saw_note") && Memory.Instance.memory["saw_note"] > 0)
            base.Take();
    }
}
