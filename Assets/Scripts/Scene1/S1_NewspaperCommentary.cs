﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using EventMaster;

public class S1_NewspaperCommentary : CommentaryItem, ICommentaryTarget
{
    public override void CommentaryOn()
    {
        if (Memory.Instance.memory.ContainsKey("intro_commentary_over") && Memory.Instance.memory["intro_commentary_over"] > 0)
            icon.SetActive(true);
    }
}
