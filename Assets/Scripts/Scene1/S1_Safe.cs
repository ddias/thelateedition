﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using EventMaster;

public class S1_Safe : Container, ILockControlHandler
{
    public bool Locked = true;
    public GameObject UnlockUI;

    public void Lock()
    {
        GetComponent<EventParticipant>().PerformQuery("locked");
        Locked = true;
    }

    public void Unlock()
    {
        GetComponent<EventParticipant>().PerformQuery("unlocked");
        Locked = false;
    }

    public override void TryCombine(GameObject obj)
    {
        if (Locked)
        {
            foreach (string key in Keys)
            {
                if (key.ToLower() == obj.name.ToLower())
                {
                    // This should effectively allow you to swap only between the two keys but still allow the safe
                    base.TryCombine(obj);
                    base.Take();
                    return;
                }
            }
            ExecuteEvents.Execute<ICombineHandler>(obj, null, (x, y) => x.CombineFail(obj));
            GetComponent<EventParticipant>().PerformQuery("locked");
        }
        else
        {
            base.TryCombine(obj);
            base.Take();
        }
    }

    public override void ExecuteInteraction(InteractionType type)
    {
        if (type == InteractionType.Take && Locked)
        {
            GetComponent<EventParticipant>().PerformQuery("locked");
            UnlockUI.SetActive(true);
        }
        else
        {
            base.ExecuteInteraction(type);
        }
    }
}
