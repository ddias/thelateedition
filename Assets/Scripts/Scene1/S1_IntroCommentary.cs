﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using EventMaster;

public class S1_IntroCommentary : CommentaryItem
{
    public GameObject NewspaperObj;

    public override void Init()
    {
        Game.Instance.PushState(GameState.Intro);
        base.Init();
        gameObject.SetActive(false);
    }

    public override void DoCommentary()
    {
        base.DoCommentary();
        ExecuteEvents.Execute<ICommentaryTarget>(NewspaperObj, null, (x, y) => x.CommentaryOn());
    }

    public override void CommentaryOn()
    {
        icon.SetActive(true);
    }
}
