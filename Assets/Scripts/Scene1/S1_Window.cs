﻿using UnityEngine;
using System.Collections.Generic;
using EventMaster;

public class S1_Window : Container
{
    protected bool hasApple = true;
    public bool hasCrowbar = false;  //inventory[] Queue<InteractableItem>
    public bool hasBowler = false;
    public bool hasFloorkey = false;

    void Update()
    {
        Debug.LogFormat("bowler_in_window: {0}", Memory.Instance.memory["bowler_in_window"]);
        Debug.LogFormat("key_in_window: {0}", Memory.Instance.memory["key_in_window"]);
        Debug.LogFormat("crowbar_in_window: {0}", Memory.Instance.memory["crowbar_in_window"]);
        Debug.LogFormat("using_apple: {0}", Memory.Instance.memory["using_apple"]);

        Debug.LogFormat("using_bowler: {0}", Memory.Instance.memory["using_bowler"]);
        Debug.LogFormat("using_floorkey: {0}", Memory.Instance.memory["using_floorkey"]);
        Debug.LogFormat("using_crowbar: {0}", Memory.Instance.memory["using_crowbar"]);
    }

    public override void TryCombine(GameObject obj)
    {
        Debug.Log("S1_Window::TryCombine()");
        if (hasApple)
        {
            foreach (string key in Keys)
            {
                Debug.Log(key);
                if (key.ToLower() == obj.name.ToLower())
                {
                    // setup the what is in
                    if (key.ToLower() == "crowbar")
                    {
                        hasCrowbar = true;
                        hasBowler = false;
                        hasFloorkey = false;
                        Memory.Instance.memory["S1_Window_hasCrowbar"] = 1;
                        Memory.Instance.memory["S1_Window_hasBowler"] = 0;
                        Memory.Instance.memory["S1_Window_hasFloorkey"] = 0;
                    }
                    else if (key.ToLower() == "bowler")
                    {
                        hasCrowbar = false;
                        hasBowler = true;
                        hasFloorkey = false;
                        Memory.Instance.memory["S1_Window_hasCrowbar"] = 0;
                        Memory.Instance.memory["S1_Window_hasBowler"] = 1;
                        Memory.Instance.memory["S1_Window_hasFloorkey"] = 0;
                    }
                    else if (key.ToLower() == "floorkey")
                    {
                        hasCrowbar = false;
                        hasBowler = false;
                        hasFloorkey = true;
                        Memory.Instance.memory["S1_Window_hasCrowbar"] = 0;
                        Memory.Instance.memory["S1_Window_hasBowler"] = 0;
                        Memory.Instance.memory["S1_Window_hasFloorkey"] = 1;
                    }
                    // TODO: This will probably cause issues in the future, but there may be no future! Live in the now!
                    base.Take();
                    base.TryCombine(obj); //  ? 
                    hasApple = false;
                    return;
                }
            }
            base.CombineFail(obj);
        }
        else
        {
            if (obj.name.ToLower() == "apple")
            {
                base.Take();
                base.TryCombine(obj);
                hasApple = true;
                hasCrowbar = false;
                hasBowler = false;
                hasFloorkey = false;
                return;
            }
            base.CombineFail(obj);
        }
    }


    public override void Take()
    {
        GetComponent<EventParticipant>().PerformQuery("take");
        if (Keys.Count == 0)
        {
            base.Take();
        }
    }
}
