﻿using UnityEngine;
using System.Collections;
using EventMaster;
using UnityEngine.EventSystems;

public class S1_Newspaper : PickupItem
{
    protected bool showingTitle = false;
    protected bool isSpinning = false;
    protected bool displayedTitle = false;
    protected bool canTake = true;

    protected Vector3 targetPos = new Vector3(-0.1F, -2, 1);
    protected Vector3 targetScale = new Vector3(7.7F, 7.7F);

    readonly float posLerpTime = 8;
    float posLerpCurrent;

    readonly float scaleLerpTime = 8;
    float scaleLerpCurrent;

    readonly float spinLerpTime = 4F;
    float spinLerpCurrent;
    int spinCount = 0;
    bool countSpin = true;

    bool waitingToTake = false;
    readonly float takeTime = 2F;
    float currentTakeTime;

    public override void OnPointerClick(PointerEventData eventData)
    {
        if (!Game.Instance.CommentaryMode || eventData.used)
        {
            // Move to this object and get it
            ExecuteEvents.Execute<IWalkToTarget>(controller, eventData, (x, y) => x.WalkTo(eventData.position));
            ExecuteEvents.Execute<IInteractHandler>(controller, eventData, (x, y) => x.Interact(InteractionType.Take, ExecuteInteraction));
        }
    }

    public override void Take()
    {
        if (Memory.Instance.memory.ContainsKey("s1_paper_active") && Memory.Instance.memory["s1_paper_active"] > 0 && !showingTitle)
        {
            showingTitle = true;
            isSpinning = true;
            GetComponent<SpriteRenderer>().sortingLayerName = "Commentary";
        }
    }

    void Update()
    {
        if (showingTitle)
        {
            if (transform.localScale.sqrMagnitude <= targetScale.sqrMagnitude)
            {
                posLerpCurrent += Time.deltaTime;
                float t = Mathf.Sin(scaleLerpCurrent / scaleLerpTime * Mathf.PI * 0.5f); // Ease in
                transform.localPosition = Vector3.Lerp(transform.localPosition, targetPos, t);

                scaleLerpCurrent += Time.deltaTime;
                t = Mathf.Sin(scaleLerpCurrent / scaleLerpTime * Mathf.PI * 0.5f); // Ease in
                transform.localScale = Vector3.Lerp(transform.localScale, targetScale, t);
            }
            if (isSpinning && spinCount < 3)
            {
                transform.Rotate(new Vector3(0, 0, 5));
                if (transform.rotation.eulerAngles.z < 270 && countSpin)
                {
                    spinCount += 1;
                    countSpin = false;
                }
                else if (transform.rotation.eulerAngles.z > 270)
                {
                    countSpin = true;
                }
            }
            else
            {
                showingTitle = false;
                displayedTitle = true;
                waitingToTake = true;
                //StartCoroutine("TakeAfter",2f);
            }
        }
        if (waitingToTake)
        {
            currentTakeTime += Time.deltaTime;
            if (currentTakeTime >= takeTime)
            {
                // Remove the Intro game state
                if (Game.Instance.State == GameState.Intro)
                    Game.Instance.PopState();
                if (canTake)
                {
                    base.Take();
                    GetComponent<SpriteRenderer>().sortingLayerName = "Default";
                    canTake = false;
                }
            }
        }
    }

    IEnumerator TakeAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        // Remove the Intro game state
        if (Game.Instance.State == GameState.Intro)
            Game.Instance.PopState();
        if (canTake)
        {
            base.Take();
            GetComponent<SpriteRenderer>().sortingLayerName = "Default";
            canTake = false;
        }
    }
}
