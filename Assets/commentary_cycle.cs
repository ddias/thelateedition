﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using EventMaster;

public class commentary_cycle : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(Memory.Instance.memory["intro_queue"]);
	}

    public GameObject toEnableOnDestroy;
    void OnDisable()
    {
        if (toEnableOnDestroy != null)
        {
            Debug.LogFormat("Enableing {0}", toEnableOnDestroy.name);
            toEnableOnDestroy.SetActive(true);
        }
    }
}
