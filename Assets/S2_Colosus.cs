﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using EventMaster;

public class S2_Colosus : Dispenser
{
    [HeaderAttribute("Behavior Flags")]
    [SerializeField]
    private bool hasbeenGunned;
    [SerializeField]
    private bool hasBeenBoozed;
    [SerializeField]
    private bool hasBeenJournaled;
    [SerializeField]
    private string gunid = "gun";
    [SerializeField]
    private string boozeid = "booze";
    [SerializeField]
    private string journalid = "journal";
    [SerializeField]
    public bool isregular;
    [SerializeField]
    public bool isrussian;
    [SerializeField]
    public bool isdwarf;

    public S2_CarKey_Transmogrify KeyTransferer;

    public SpriteRenderer DoorImageTarget;
    public Sprite DoorImage;
    public S2_Exit Door;
    // x.SwapImage(res.source,imageswap[0])
    public override void TryCombine(GameObject obj)
    {
        Debug.Log("S1_Collosus::TryCombine()");
        MonoBehaviour script = obj.GetComponent<InteractableItem>();
        if (script == null)
            script = obj.GetComponent<InventoryItem>();
        if (script != null)
        {
            //regular and gun
            if (isregular)
            {
                if (obj.name.ToLower() == gunid.ToLower())
                {
                    hasbeenGunned = true;
                    CombineSucessAndTakeKeys(obj);
                    KeyTransferer.Unhide();
                    return;
                }
            }
            //russian and gift
            else if (isrussian)
            {
                //Debug.LogFormat("combining {0} and {1}", obj.name, boozeid.ToLower());
                if (obj.name.ToLower() == boozeid.ToLower())
                {
                    hasBeenBoozed = true;
                    Memory.Instance.memory["colosus_boozed"] = 1;
                    CombineSuccess(obj);
                    return;
                }
            }
            //drunk dwarf and journal
            else if(isdwarf)
            {
                if (hasBeenBoozed)
                {
                    if (obj.name.ToLower() == journalid.ToLower())
                    {
                        hasBeenJournaled = true;
                        CombineSuccess(obj);
                        //swap image
                        DoorImageTarget.sprite = DoorImage;
                        // set door exitable
                        Door.exitable = true;
                        // Door.exitable = true;
                        return;
                    }
                }
            }
            CombineFail(obj);
        }
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        ExecuteEvents.Execute<IInteractHandler>(controller, eventData, (x, y) => x.Interact(InteractionType.Talk, ExecuteInteraction, false));
    }

    //public override void CombineSuccess(GameObject item)
    //{
    //    Debug.Log("DropOffItem::CombineSuccess()");
    //    Memory.Instance.memory["using_" + item.name.ToLower()] = 1;
    //    ep.PerformQuery("combine");
    //    Memory.Instance.memory["using_" + item.name.ToLower()] = 0;
    //    ExecuteEvents.Execute<ICombineHandler>(item, null, (x, y) => x.CombineSuccess(gameObject));
    //}

    //public override void CombineFail(GameObject item)
    //{
    //    Debug.Log("DropOffItem::CombineFail()");
    //    Memory.Instance.memory["using_" + item.name.ToLower()] = 1;
    //    ep.PerformQuery("combine");
    //    Memory.Instance.memory["using_" + item.name.ToLower()] = 0;
    //    ExecuteEvents.Execute<ICombineHandler>(item, null, (x, y) => x.CombineFail(gameObject));
    //}

    public GameObject StoredKeys;

    public void CombineSucessAndTakeKeys(GameObject item)
    {

        if (StoredKeys != null)
        {
            InteractableItem ItemInteraction = StoredKeys.GetComponent<InteractableItem>();
            if (ItemInteraction != null)
            {
                //GetComponent<EventParticipant>().PerformQuery("take");
                ItemInteraction.Take();
                StoredKeys = null;
                CombineSuccess(item);
            }
            else { Debug.Log("Tried to take from non Interactable"); }
        }

    }
}


